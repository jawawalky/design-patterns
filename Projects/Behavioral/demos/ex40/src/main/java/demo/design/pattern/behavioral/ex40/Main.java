/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex40;

import demo.design.pattern.behavioral.ex40.impl.Line2BlankConverter;
import demo.design.pattern.behavioral.ex40.impl.LineBreak2BlankConverter;
import demo.util.Demo;

/**
 * This demo shows you, how to use template methods.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		LineBreak2BlankConverter c1 =
			new LineBreak2BlankConverter(
				"data/input.txt",
				"data/output.txt"
			);
		c1.convert();
		
		Line2BlankConverter c2 =
			new Line2BlankConverter("input.txt", null);
		c2.convert();
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
