/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex40.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import demo.design.pattern.behavioral.ex40.Document;
import demo.design.pattern.behavioral.ex40.DocumentConverter;
import demo.design.pattern.behavioral.ex40.TextDocument;
import demo.util.Demo;

/**
 * A converter, which converts text documents of type
 * <pre>
 * 	This
 * 	is
 * 	the
 * 	text.
 * </pre>
 * into text documents of type
 * <pre>
 * 	This is the text.
 * </pre>
 * 
 * @author Franz Tost
 *
 */
public class Line2BlankConverter extends DocumentConverter {
	
	// constructors
	// ........................................................................

	public Line2BlankConverter(
		final String inputLocation,
		final String outputLocation
	) {
		
		super(inputLocation, outputLocation);
		
	}

	// methods
	// ........................................................................

	@Override
	protected Document convertDocument(final Document inputDocument) {
		
		TextDocument inDoc = (TextDocument) inputDocument;
		TextDocument outDoc = new TextDocument();
		
		StringTokenizer tokenizer = new StringTokenizer(inDoc.getLine(0), "|");
		while (tokenizer.hasMoreTokens()) {
			
			outDoc.addLine(tokenizer.nextToken());
			
		} // while
		
		return outDoc;
		
	}

	@Override
	protected Document loadDocument() {
		
		try (
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
					this.getClass().getResourceAsStream(
						this.inputLocation
					)
				)
			);
		) {
			
			String line = in.readLine();
			
			TextDocument document = new TextDocument();
			document.addLine(line);
			
			return document;
			
		} // try-with-resources
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		} // catch
		
	}

	@Override
	protected void saveDocument(final Document outputDocument) {
		
		TextDocument document = (TextDocument) outputDocument;
		for (int i = 0; i < document.getLineCount(); i++) {
			
			Demo.println(document.getLine(i));
			
		} // for
			
	}

}
