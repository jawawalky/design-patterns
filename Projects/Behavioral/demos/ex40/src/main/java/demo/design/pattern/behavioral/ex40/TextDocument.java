/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex40;

import java.util.ArrayList;
import java.util.List;

/**
 * As simple text document.
 * 
 * @author Franz Tost
 *
 */
public class TextDocument implements Document {
	
	// fields
	// ........................................................................
	
	private List<String> content = new ArrayList<String>();
	
	// methods
	// ........................................................................
	
	public void addLine(String line) {
		
		this.content.add(line);
		
	}
	
	public int getLineCount() {
		
		return this.content.size();
		
	}
	
	public String getLine(int index) {
		
		return this.content.get(index);
		
	}

}
