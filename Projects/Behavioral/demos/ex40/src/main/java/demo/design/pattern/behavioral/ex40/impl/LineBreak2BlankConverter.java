/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex40.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import demo.design.pattern.behavioral.ex40.Document;
import demo.design.pattern.behavioral.ex40.DocumentConverter;
import demo.design.pattern.behavioral.ex40.TextDocument;
import demo.util.Demo;

/**
 * Reads a text document from the file system. The lines of the input document
 * should be concatenated, only separated by a blank. The output document
 * should be written to the file system.
 * 
 * @author Franz Tost
 *
 */
public class LineBreak2BlankConverter extends DocumentConverter {
	
	// constructors
	// ........................................................................

	public LineBreak2BlankConverter(
		final String inputLocation,
		final String outputLocation
	) {
		
		super(inputLocation, outputLocation);
		
	}

	@Override
	protected Document convertDocument(final Document inputDocument) {
		
		StringBuilder stringBuilder = new StringBuilder();
		
		TextDocument inDoc = (TextDocument) inputDocument;
		
		for (int i = 0; i < inDoc.getLineCount(); i++) {
			
			stringBuilder.append(inDoc.getLine(i)).append(" ");
			
		} // for
		
		TextDocument outDoc = new TextDocument();
		outDoc.addLine(stringBuilder.toString());
		return outDoc;
		
	}

	@Override
	protected Document loadDocument() {
		
		try (
			BufferedReader in = 
				new BufferedReader(new FileReader(this.inputLocation));
		) {
			
			TextDocument document = new TextDocument();
			
			String line = null;
			
			while ((line = in.readLine()) != null) {
				
				document.addLine(line);
				
			} // while
			
			return document;
			
		} // try-with-resources
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		} // catch
		
	}

	@Override
	protected void saveDocument(Document outputDocument) {
		
		try (
			PrintWriter out =
				new PrintWriter(new FileWriter(this.outputLocation));
		) {
			
			TextDocument document = (TextDocument) outputDocument;
			
			for (int i = 0; i < document.getLineCount(); i++) {
				
				out.println(document.getLine(i));
				
			} // for
			
		} // try-with-resources
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			
		} // catch
		
	}

}
