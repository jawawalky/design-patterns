/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20.impl;

import demo.design.pattern.behavioral.ex20.Message;
import demo.design.pattern.behavioral.ex20.MessageHandler;
import demo.design.pattern.behavioral.ex20.Message.Priority;
import demo.util.Demo;

/**
 * A handler, which filters message with too low priority.
 * 
 * @author Franz Tost
 *
 */
public class PriorityHandler extends AbstractMessageHandler {
	
	// fields
	// ........................................................................
	
	/**
	 * The priority the message should at least have to be accepted.
	 */
	private Priority minPriority = Priority.LOW;

	// constructors
	// ........................................................................
	
	/**
	 * Creates the handler.
	 * 
	 * @param previousHandler the previous handler.
	 * @param minPriority the required minimum priority.
	 */
	public PriorityHandler(MessageHandler previousHandler, Priority minPriority) {
		
		super(previousHandler);
		
		this.minPriority = minPriority;
		
	}

	// methods
	// ........................................................................
	
	/**
	 * Lets a message pass, if its priority is greater or equal to the
	 * set minimum priority.
	 * 
	 * @param message the incoming message.
	 * 
	 * @return <code>true</code>, if the message should be passed to the next
	 * 		handler.<br/>
	 * 		<code>false</code>, if not.
	 */
	@Override
	public boolean processMessage(Message message) {
		
		if (message.getPriority().compareTo(minPriority) >= 0) {
			
			return true;
			
		}
		else {
			
			Demo.log("BLOCKED! " + message + " - Priority too low!");
			return false;
			
		} // if
		
	}

}
