/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20.impl;

import java.util.HashMap;
import java.util.Map;

import demo.design.pattern.behavioral.ex20.Message;
import demo.design.pattern.behavioral.ex20.MessageHandler;
import demo.design.pattern.behavioral.ex20.Recipient;
import demo.util.Demo;

/**
 * A handler, which knows the recipients and distributes incoming messages
 * to its addressees.
 * 
 * @author Franz Tost
 *
 */
public class DistributionHandler extends AbstractMessageHandler {

	// fields
	// ........................................................................
	
	/**
	 * The possible recipients of messages.
	 */
	private Map<String, Recipient> recipients = new HashMap<String, Recipient>();
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates the handler and registers the recipients.
	 * 
	 * @param previousHandler the previous handler
	 * @param recipients the possible recipients for messages.
	 */
	public DistributionHandler(MessageHandler previousHandler, Recipient... recipients) {
		
		super(previousHandler);
		
		for (Recipient recipient : recipients) {
			
			this.addRecipient(recipient);
			
		} // for
		
	}

	// methods
	// ........................................................................
	
	/**
	 * Adds aq new recipient to the handler.
	 * 
	 * @param recipient the recipient.
	 */
	public void addRecipient(Recipient recipient) {
		
		this.recipients.put(recipient.getName(), recipient);
		
	}
	
	/**
	 * Delivers an incoming message to its addressee.
	 * 
	 * @param message the incoming message.
	 * 
	 * @return <code>true</code>, if the message could be delivered to its
	 * 		recipient.<br/>
	 * 		<code>false</code>, if not.
	 */
	@Override
	public boolean processMessage(Message message) {
		
		Recipient recipient = this.recipients.get(message.getRecipientName());
		if (recipient != null) {
			
			recipient.receive(message);
			return true;
			
		}
		else {
			
			Demo.log("ERROR! Unknown recipient '" + message.getRecipientName() + "'!"); 
			return false;
			
		} // if
		
	}

}
