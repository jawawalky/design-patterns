/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import demo.design.pattern.behavioral.ex20.Message;
import demo.design.pattern.behavioral.ex20.MessageHandler;
import demo.util.Demo;

/**
 * A handler, which checks the integrity of the received message.
 * 
 * @author Franz Tost
 *
 */
public class IntegrityHandler extends AbstractMessageHandler {

	// constructors
	// ........................................................................
	
	/**
	 * Creates the handler.
	 * 
	 * @param previousHandler the previous handler.
	 */
	public IntegrityHandler(MessageHandler previousHandler) {
		
		super(previousHandler);
		
	}

	// methods
	// ........................................................................
	
	/**
	 * Applies a MD5 message digest to the received message content and
	 * compares it to the received message digest.
	 * 
	 * @param message the incoming message.
	 * 
	 * @return <code>true</code>, if the message is o.k.<br/>
	 * 		<code>false</code>, if the message was manipulated.
	 */
	@Override
	public boolean processMessage(Message message) {
		
		try {
			
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] digest = messageDigest.digest(message.getContent());
			
			if (MessageDigest.isEqual(digest, message.getDigest())) {
				
				return true;
				
			}
			else {
				
				Demo.log("BLOCKED! " + message + " - The message was manipulated!");
				return false;
				
			} // if
			
		} // try
		catch (NoSuchAlgorithmException e) {
			
			Demo.log(e);
			return false;
			
		} // catch
		
	}

}
