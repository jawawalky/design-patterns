/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20;

import demo.design.pattern.behavioral.ex20.Message.Priority;
import demo.design.pattern.behavioral.ex20.impl.DistributionHandler;
import demo.design.pattern.behavioral.ex20.impl.IntegrityHandler;
import demo.design.pattern.behavioral.ex20.impl.PriorityHandler;
import demo.util.Demo;

/**
 * This demo shows you, how the chain of responsibility pattern works.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");

		MessageHandler messageHandler = this.createMessageHandlerChain();

		this.sendMessage(messageHandler,
			"Bob",
			Priority.NORMAL,
			"Hi Bob! How are you?");
		this.sendMessage(messageHandler,
			"Sue",
			Priority.HIGH,
			"Hi Sue! This is very important!");
		this.sendMessage(messageHandler,
			"Ann",
			Priority.LOW,
			"Are you already sleeping?");
		this.sendMessage(messageHandler,
			"Bobby",
			Priority.NORMAL,
			"Hi Bob! Are you still there?");
		this.sendMessage(messageHandler,
			"Bob",
			Priority.NORMAL,
			"Hi Bob! Are you still there?");

		this.sendManipulatedMessage(messageHandler,
			"Joe",
			Priority.HIGH,
			"Joe the secret code is 12345!",
			"Joe the secret code is 98765!");

		Demo.log("Finished.");

	}

	/**
	 * Creates the recipients and the message handler chain.
	 * 
	 * @return The message handler chain.
	 */
	public MessageHandler createMessageHandlerChain() {

		// Create recipients
		//
		Recipient bob = new Recipient("Bob");
		Recipient ann = new Recipient("Ann");
		Recipient sue = new Recipient("Sue");
		Recipient joe = new Recipient("Joe");

		// Create message handlers and build chain
		//
		MessageHandler integrityHandler = new IntegrityHandler(null);
		MessageHandler priorityHandler = new PriorityHandler(integrityHandler,
			Priority.NORMAL);
		new DistributionHandler(priorityHandler,
			bob,
			ann,
			sue,
			joe);

		return integrityHandler;

	}

	/**
	 * Send a message to the specified recipient.
	 * 
	 * @param messageHandler the message handler chain.
	 * @param recipientName the name of the recipient.
	 * @param priority the priority of the message.
	 * @param messageText the message text.
	 */
	private void sendMessage(MessageHandler messageHandler,
		String recipientName,
		Priority priority,
		String messageText) {

		Message message = new Message(recipientName,
			priority,
			messageText.getBytes());
		messageHandler.handleMessage(message);

	}

	/**
	 * Send a message to the specified recipient. We simulate a manipulation
	 * of the message, by setting a new message text.
	 * 
	 * @param messageHandler the message handler chain.
	 * @param recipientName the name of the recipient.
	 * @param priority the priority of the message.
	 * @param messageText the message text.
	 * @param manipulatedMessageText the changed message text.
	 */
	private void sendManipulatedMessage(MessageHandler messageHandler,
		String recipientName,
		Priority priority,
		String messageText,
		String manipulatedMessageText) {

		Message message = new Message(recipientName,
			priority,
			messageText.getBytes());
		message.setContent(manipulatedMessageText.getBytes());
		messageHandler.handleMessage(message);

	}

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
