/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20.impl;

import demo.design.pattern.behavioral.ex20.Message;
import demo.design.pattern.behavioral.ex20.MessageHandler;

/**
 * An abstract base class for implementations of the {@link MessageHandler}
 * interface.
 * 
 * @author Franz Tost
 *
 */
public abstract class AbstractMessageHandler implements MessageHandler {
	
	// fields
	// ........................................................................
	
	/**
	 * The message handler to be applied after this handler.
	 */
	private MessageHandler nextHandler;
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates the message handler.
	 * 
	 * @param previousHandler the message, which comes before this handler in
	 * 		the chain.<br/>
	 * 		Pass <code>null</code>, if this is the first handler of the chain.
	 */
	public AbstractMessageHandler(MessageHandler previousHandler) {
		
		super();
		
		if (previousHandler != null) {
			
			previousHandler.setNextHandler(this);
			
		} // if
		
	}

	// methods
	// ........................................................................
	
	/**
	 * Handles an incoming message. It delegates the processing to the
	 * method {@link #processMessage(Message)}. If that method returns
	 * <code>true</code> and a next message handler exists, then the message
	 * is passed on to the following handler for further processing.
	 * Otherwise the chain stops processing the message.
	 * 
	 * @param message the incoming message.
	 */
	public void handleMessage(Message message) {
		
		if (this.processMessage(message) && (this.nextHandler != null)) {
			
			this.nextHandler.handleMessage(message);
			
		} // if

	}

	/**
	 * Processes the message.
	 * 
	 * @param message the incoming message.
	 * 
	 * @return <code>true</code>, if the message should be passed to the
	 * 		next handler in the chain.<br/>
	 * 		<code>false</code>, if not.
	 */
	public abstract boolean processMessage(Message message);
	
	public void setNextHandler(MessageHandler handler) {
		
		this.nextHandler = handler;
		
	}

}
