/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

/**
 * A remote control, which allows us to turn a TV set on and off and which
 * let's us select a channel.
 * 
 * @author Franz Tost
 *
 */
public class RemoteControl {
	
	// fields /////
	
	private Command on;
	private Command off;
	private Command chn1;
	private Command chn2;
	private Command chn3;
	private Command chn4;
	private Command chn5;
	
	
	// constructors /////
	
	public RemoteControl(final TV tv) {
		
		super();
		
		this.on   = () -> tv.turnOn();
		this.off  = () -> tv.turnOff();
		this.chn1 = () -> tv.selectChannel(1);
		this.chn2 = () -> tv.selectChannel(2);
		this.chn3 = () -> tv.selectChannel(3);
		this.chn4 = () -> tv.selectChannel(4);
		this.chn5 = () -> tv.selectChannel(5);
		
	}
	
	
	// methods /////
	
	public void pressOn()   { this.execute(this.on); }
	public void pressOff()  { this.execute(this.off); }
	public void pressChn1() { this.execute(this.chn1); }
	public void pressChn2() { this.execute(this.chn2); }
	public void pressChn3() { this.execute(this.chn3); }
	public void pressChn4() { this.execute(this.chn4); }
	public void pressChn5() { this.execute(this.chn5); }
	
	private void execute(final Command command) {
		
		command.execute();
		
	}
	
}
