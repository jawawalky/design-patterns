/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

import demo.util.Demo;

/**
 * A TV set, which can be turned on and off and where we can choose channels.
 * 
 * @author Franz Tost
 *
 */
public class TV {
	
	// fields /////
	
	private boolean on;
	
	private int channel = 1;
	
	
	// methods /////
	
	public void turnOn() {
	
		if (!this.on) {
			
			this.on = true;
			Demo.log("TV has been turned on!");
			
		} // if
		
	}
	
	public void turnOff() {
		
		if (this.on) {
			
			this.on = false;
			Demo.log("TV has been turned off!");
			
		} // if
		
	}
	
	public void selectChannel(final int n) {
		
		if (this.on) {
			
			this.channel = n;
			Demo.log("TV is now showing channel " + this.channel + ".");
			
		} // if
		
	}
	
}
