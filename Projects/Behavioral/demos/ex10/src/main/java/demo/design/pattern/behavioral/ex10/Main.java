/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

import demo.util.Demo;

/**
 * This demo shows you, how to implement and use the <i>Command</i> pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final TV            tv            = new TV();
		final RemoteControl remoteControl = new RemoteControl(tv);
		final User          fred          = new User();
		
		fred.startTV(remoteControl);
		Demo.sleep(500);
		fred.selectChannel(3);
		Demo.sleep(2000);
		fred.selectChannel(2);
		Demo.sleep(1000);
		fred.selectChannel(5);
		Demo.sleep(4000);
		fred.stopTV();
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
