/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

/**
 * A user operating a TV set by a remote control.
 * 
 * @author Franz Tost
 *
 */
public class User {
	
	// fields /////
	
	private RemoteControl remoteControl;

	
	// methods /////
	
	public void startTV(final RemoteControl remoteControl) {
		
		this.remoteControl = remoteControl;
		this.remoteControl.pressOn();
		
	}
	
	public void stopTV() {
		
		this.remoteControl.pressOff();
		this.remoteControl = null;
		
	}
	
	public void selectChannel(final int n) {
		
		if (this.remoteControl != null) {
			
			switch (n) {
			
			case 1: this.remoteControl.pressChn1(); break;
			case 2: this.remoteControl.pressChn2(); break;
			case 3: this.remoteControl.pressChn3(); break;
			case 4: this.remoteControl.pressChn4(); break;
			case 5: this.remoteControl.pressChn5(); break;
			
			} // switch
			
		} // if
		
	}
	
}
