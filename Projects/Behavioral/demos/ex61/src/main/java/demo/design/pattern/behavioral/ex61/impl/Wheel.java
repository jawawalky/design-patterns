/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

/**
 * A wheel.
 * 
 * @author Franz Tost
 *
 */
public class Wheel extends AbstractPart {

	// constructors
	// ........................................................................

	public Wheel(String name) {
		
		super(name, 22.90, 10.87);
		
	}

}
