/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

import java.util.ArrayList;
import java.util.List;

import demo.design.pattern.behavioral.ex61.Part;
import demo.design.pattern.behavioral.ex61.PartVisitor;

/**
 * A car is an object, which is composed of many parts.
 * 
 * @author Franz Tost
 *
 */
public class Car {
	
	// fields
	// ........................................................................

	private String number;
	
	private Chassis chassis;
	
	private List<Airbag> airbags = new ArrayList<Airbag>();
	
	private List<Wheel> wheels = new ArrayList<Wheel>();
	
	private List<Door> doors = new ArrayList<Door>();
	
	// constructors
	// ........................................................................

	public Car(String number) {
		
		this.number = number;
		
	}
	
	// methods
	// ........................................................................

	public String getNumber() {
		
		return this.number;
		
	}
	
	public void setChassis(Chassis chassis) {
		
		this.chassis = chassis;
		
	}
	
	public void addAirbag(Airbag airbag) {
		
		this.airbags.add(airbag);
		
	}

	public void addWheel(Wheel wheel) {
		
		this.wheels.add(wheel);
		
	}

	public void addDoor(Door door) {
		
		this.doors.add(door);
		
	}
	
	/**
	 * Calculates the price of the car. We use the {@link PriceFinder} to
	 * determine the total price of the whole car. The {@link PriceFinder}
	 * implements the {@link PartVisitor}.
	 * 
	 * @return The price.
	 */
	public double getPrice() {
		
		PriceFinder priceFinder = new PriceFinder();
		this.accept(priceFinder);
		return priceFinder.getTotalPrice();
		
	}
	
	/**
	 * Calculates the weight of the car. We use the {@link WeightFinder} to
	 * determine the total weight of the whole car. The {@link WeightFinder}
	 * implements the {@link PartVisitor}.
	 * 
	 * @return The weight.
	 */
	public double getWeight() {
		
		WeightFinder weightFinder = new WeightFinder();
		this.accept(weightFinder);
		return weightFinder.getTotalWeight();
		
	}
	
	/**
	 * Returns the number of parts of the car. We use the {@link PartCounter}
	 * to determine the total weight of the whole car. The
	 * {@link PartCounter} implements the {@link PartVisitor}.
	 * 
	 * @return The part count.
	 */
	public int getPartCount() {
		
		PartCounter partCounter = new PartCounter();
		this.accept(partCounter);
		return partCounter.getCount();
		
	}
	
	/**
	 * Prints a list of all parts of the car. We use the {@link PartList} to
	 * print all parts. The {@link PartList} implements the {@link PartVisitor}.
	 */
	public void getPartList() {
		
		PartList partList = new PartList();
		this.accept(partList);
		
	}
	
	/**
	 * Lets a visitor visit all parts of the car.
	 * 
	 * @param visitor the visitor.
	 */
	private void accept(PartVisitor visitor) {
		
		this.chassis.accept(visitor);
		this.acceptList(visitor, this.airbags);
		this.acceptList(visitor, this.wheels);
		this.acceptList(visitor, this.doors);
		
	}

	/**
	 * A helper method, which applies the visitor to all parts of a list.
	 * 
	 * @param visitor the visitor.
	 * @param parts the list of parts.
	 */
	private void acceptList(PartVisitor visitor, List<? extends Part> parts) {
		
		for (Part part : parts) {
			
			part.accept(visitor);
			
		} // for
		
	}

}
