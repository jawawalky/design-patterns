/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

import demo.design.pattern.behavioral.ex61.Part;
import demo.design.pattern.behavioral.ex61.PartVisitor;

/**
 * @author Franz Tost
 *
 */
public class PartCounter implements PartVisitor {
	
	// fields
	// ........................................................................
	
	private int count;

	// methods
	// ........................................................................
	
	public void visit(Part part) {
		
		this.count++;

	}
	
	public int getCount() {
		
		return this.count;
		
	}

}
