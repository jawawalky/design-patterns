/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61;

/**
 * The interface part visitor must implement.
 * 
 * @author Franz Tost
 *
 */
public interface PartVisitor {
	
	// methods
	// ........................................................................
	
	/**
	 * Called, when the visitor visits some part. The part is passed as
	 * a parameter to the method.
	 * 
	 * @param part the part visited.
	 */
	void visit(Part part);

}
