/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61;

import demo.design.pattern.behavioral.ex61.impl.Airbag;
import demo.design.pattern.behavioral.ex61.impl.Car;
import demo.design.pattern.behavioral.ex61.impl.Chassis;
import demo.design.pattern.behavioral.ex61.impl.Door;
import demo.design.pattern.behavioral.ex61.impl.Wheel;
import demo.util.Demo;

/**
 * This demo shows you, how use the <i>Visitor</i> pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log("Car 1:");
		
		Car car1 = new Car("1");
		car1.setChassis(new Chassis("D204", 566.0, 732.51));
		car1.addAirbag(new Airbag("Left Front Airbag"));
		car1.addAirbag(new Airbag("Right Front Airbag"));
		car1.addWheel(new Wheel("Left Front Wheel"));
		car1.addWheel(new Wheel("Right Front Wheel"));
		car1.addWheel(new Wheel("Left Rear Wheel"));
		car1.addWheel(new Wheel("Right Rear Wheel"));
		car1.addDoor(new Door("Left Front Door"));
		car1.addDoor(new Door("Right Front Door"));
		
		Demo.log("  Price:      " + car1.getPrice());
		Demo.log("  Weight:     " + car1.getWeight());
		Demo.log("  Part Count: " + car1.getPartCount());
		Demo.log("  Part List:");
		car1.getPartList();
		Demo.println();
		
		
		Demo.log("Car 2:");
		
		Car car2 = new Car("2");
		car2.setChassis(new Chassis("2002", 1080.0, 944.39));
		car2.addAirbag(new Airbag("Left Front Airbag"));
		car2.addAirbag(new Airbag("Right Front Airbag"));
		car2.addAirbag(new Airbag("Left Rear Airbag"));
		car2.addAirbag(new Airbag("Right Rear Airbag"));
		car2.addWheel(new Wheel("Left Front Wheel"));
		car2.addWheel(new Wheel("Right Front Wheel"));
		car2.addWheel(new Wheel("Left Rear Wheel"));
		car2.addWheel(new Wheel("Right Rear Wheel"));
		car2.addWheel(new Wheel("Spare Wheel"));
		car2.addDoor(new Door("Left Front Door"));
		car2.addDoor(new Door("Right Front Door"));
		car2.addDoor(new Door("Left Rear Door"));
		car2.addDoor(new Door("Right Rear Door"));
		
		Demo.log("  Price:      " + car2.getPrice());
		Demo.log("  Weight:     " + car2.getWeight());
		Demo.log("  Part Count: " + car2.getPartCount());
		Demo.log("  Part List:");
		car2.getPartList();
		Demo.println();
		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
