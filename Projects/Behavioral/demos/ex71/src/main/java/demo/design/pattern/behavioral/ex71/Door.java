/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex71;

/**
 * A door, which can be open or closed.
 * 
 * @author Franz Tost
 */
public class Door {
	
	// inner classes
	// ........................................................................
	
	private interface State {
		
		// methods
		// ....................................................................

		void open();
		
		void close();

	}
	
	private class Open implements State {
		
		// methods
		// ....................................................................

		public void open() {
			
			// Do nothing!
			
		}

		public void close() {
			
			state = new Closed();
			
		}

	}
	
	private class Closed implements State {
		
		// methods
		// ....................................................................

		public void open() {
			
			state = new Open();
			
		}

		public void close() {
			
			// Do nothing!
			
		}

	}
	
	// fields
	// ........................................................................

	private State state = new Closed();

	// methods
	// ........................................................................

	public void open() {
		
		this.state.open();
		
	}

	public void close() {
		
		this.state.close();
		
	}
	
	public boolean isOpen() {
		
		return (this.state instanceof Open);
		
	}

	public boolean isClosed() {
		
		return (this.state instanceof Closed);
		
	}
	
	@Override
	public String toString() {
		
		return "The door is " + (this.isOpen() ? "open" : "closed") + ".";
		
	}

}