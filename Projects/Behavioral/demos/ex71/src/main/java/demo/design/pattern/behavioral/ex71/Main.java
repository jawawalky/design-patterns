/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex71;

import demo.util.Demo;

/**
 * The application demonstrates the state pattern.
 * 
 * @author Franz Tost
 */
public class Main {

	// fields
	// ........................................................................
	
	// constructors
	// ........................................................................

	private Main() {
		
		Demo.log("Running demo ...");
		
		Door door = new Door();
		
		Demo.log("A) " + door.toString()); 
		door.open();
		Demo.log("B) " + door.toString()); 
		door.close();
		Demo.log("C) " + door.toString()); 
		door.close();
		Demo.log("D) " + door.toString()); 
		door.open();
		Demo.log("E) " + door.toString());
		
		Demo.log("Finished.");
		
	}

	// methods
	// ........................................................................
	
	/**
	 * Runs the application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		new Main();
		
	}
	
}