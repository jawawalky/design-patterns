/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * The visitor can visit a person an his/her parts.
 * 
 * @author Franz Tost
 *
 */
public interface Visitor {
	
	// methods
	// ........................................................................
	
	void visit(Person person);

	void visit(Blood blood);

	void visit(Heart heart);

	void visit(Lung lung);

}
