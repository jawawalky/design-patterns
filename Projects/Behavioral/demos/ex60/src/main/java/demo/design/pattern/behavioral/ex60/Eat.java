/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2013 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * A visitor, which adds an operation to our person.
 * <br/>
 * <b>Note:</b>
 * <blockquote>
 * 	The person must give access to internal elements such as the
 * 	blood pressure etc. by setter-methods. 
 * </blockquote>
 * 
 * @author Franz Tost
 *
 */
public class Eat implements Visitor {
	
	// methods
	// ........................................................................

	@Override public void visit(Person person) {
		
		// Do nothing

	}

	@Override public void visit(Blood blood) {
		
		blood.setAlcohol(0.0);
		blood.setPressure(blood.getPressure() + 10);
		
	}

	@Override public void visit(Heart heart) {
		
		heart.setPulse(120);
		
	}

	@Override public void visit(Lung lung) {
		
		lung.setRespiration(50);

	}

}
