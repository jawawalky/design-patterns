/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * @author Franz Tost
 *
 */
public class Lung implements Visitable {
	
	// fields
	// ........................................................................
	
	private int respiration = 15;

	// methods
	// ........................................................................
	
	@Override public void accept(Visitor visitor) { visitor.visit(this); }

	public int  getRespiration()                { return this.respiration;        }
	public void setRespiration(int respiration) { this.respiration = respiration; }
	
}
