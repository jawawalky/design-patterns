/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

import demo.util.Demo;

/**
 * This demo shows you, how different factories may add flexibility or
 * speed up the performance of your program.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Person franz  = new Person("Franz");
		Doctor doctor = new Doctor();
		
		franz.run();
		franz.run();
		
		franz.accept(doctor);
		Demo.log(doctor.getDiagnosis());
		
		franz.drink();
		franz.drink();
		franz.drink();
		franz.drink();
		
		franz.accept(doctor);
		Demo.log(doctor.getDiagnosis());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
