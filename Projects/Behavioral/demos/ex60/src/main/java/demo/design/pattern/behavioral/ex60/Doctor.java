/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * @author Franz Tost
 *
 */
public class Doctor implements Visitor {
	
	// fields
	// ........................................................................
	
	private StringBuilder diagnosis;

	// methods
	// ........................................................................

	@Override public void visit(Person person) {
		
		this.diagnosis = new StringBuilder();
		this.diagnosis.append("Hi ").append(person.getName()).append("\n");

	}

	@Override public void visit(Blood blood) {
		
		if (blood.getAlcohol() > 0.5) {
			
			this.diagnosis.append("Don't drive!\n");
			
		} // if
		
		if (blood.getAlcohol() > 1.2) {
			
			this.diagnosis.append("Stop drinking!\n");
			
		} // if
		
		if (blood.getAlcohol() > 3.0) {
			
			this.diagnosis.append("Go to the hospital!\n");
			
		} // if
		
		if (blood.getAlcohol() > 5.0) {
			
			this.diagnosis.append("You should be dead?!\n");
			
		} // if
		
		
		if (blood.getPressure() > 100.0) {
			
			this.diagnosis.append("Take those pink pills!\n");
			
		} // if
		
		if (blood.getPressure() > 130.0) {
			
			this.diagnosis.append("I'll better call an ambulance!\n");
			
		} // if
		
	}

	@Override public void visit(Heart heart) {
		
		if (heart.getPulse() > 100) {
			
			this.diagnosis.append("Calm down!\n");
			
		} // if

	}

	@Override public void visit(Lung lung) {
		
		if (lung.getRespiration() > 40) {
			
			this.diagnosis.append("Take your time!\n");
			
		} // if

	}
	
	public String getDiagnosis() {
		
		return this.diagnosis.toString();
		
	}

}
