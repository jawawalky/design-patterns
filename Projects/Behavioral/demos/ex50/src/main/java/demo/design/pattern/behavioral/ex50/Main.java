/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex50;

import demo.util.Demo;

/**
 * This demo shows you, how the <i>Observer</i> pattern works.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		Logger logger = new Logger();
		
		ConsoleAppender consoleAppender = new ConsoleAppender();
		FileAppender    fileAppender    = new FileAppender();
		
		logger.addObserver(consoleAppender);
		logger.addObserver(fileAppender);
		
		logger.log("Hello");
		logger.log("World!");
		
		logger.deleteObserver(fileAppender);
		logger.log("Message only for the console.");
		
		logger.addObserver(fileAppender);
		logger.deleteObserver(consoleAppender);
		logger.log("Message only for the file.");
		
		logger.addObserver(consoleAppender);
		logger.log("And now on both again.");
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
