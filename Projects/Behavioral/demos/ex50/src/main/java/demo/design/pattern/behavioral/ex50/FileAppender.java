/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex50;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * An observer, which writes the log message to a log file.
 * 
 * @author Franz Tost
 *
 */
public class FileAppender implements Observer {
	
	// methods
	// ........................................................................

	/**
	 * Takes the second argument as the log message and writes it to a
	 * log file.
	 * 
	 * @param o the observable object.
	 * @param arg the message.
	 * 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable o, Object arg) {
		
		try {
			
			BufferedWriter out = new BufferedWriter(new FileWriter("logs/demo.log", true));
			
			out.write(arg.toString());
			out.newLine();
			
			out.flush();
			out.close();
			
		} // try
		catch (IOException e) {
		} // catch

	}

}
