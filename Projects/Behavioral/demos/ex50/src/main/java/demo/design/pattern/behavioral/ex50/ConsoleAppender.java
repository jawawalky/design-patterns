/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex50;

import java.util.Observable;
import java.util.Observer;

import demo.util.Demo;

/**
 * An observer, which logs the received messages to the console.
 * 
 * @author Franz Tost
 *
 */
public class ConsoleAppender implements Observer {
	
	// methods
	// ........................................................................

	/**
	 * We assume that the second argument is the message to be printed.
	 * 
	 * @param o the observable object.
	 * @param arg the message.
	 * 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable o, Object arg) {
		
		Demo.log(arg.toString());

	}

}
