/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex50;

import java.util.Observable;

/**
 * The logger class is derived from the class <code>Observable</code>. This
 * allows us to register <code>Observer</code> objects, which can be notified
 * about changes that happen in the observable object.
 * 
 * @author Franz Tost
 *
 */
public class Logger extends Observable {
	
	// methods
	// ........................................................................
	
	/**
	 * Writes a log message. Where the message is going to, depends on the
	 * registered observers.
	 * 
	 * @param message the message that should be logged.
	 */
	public void log(String message) {
		
		this.setChanged();
		this.notifyObservers(message);
		
	}

}
