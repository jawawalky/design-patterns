/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex01;

import demo.util.Demo;

/**
 * An abstract base class for chart printers.
 *  
 * @author Franz Tost
 *
 */
public abstract class AbstractChartPrinter implements ChartPrinter {
	
	// methods
	// ........................................................................

	/**
	 * Prints 20 rows and as many column as there are values. If the value
	 * is exceeds the limit, then a '|' is printed, otherwise a blank.
	 * 
	 * @param values the values.
	 * 
	 * @see demo.design.pattern.behavioral.ex01.ChartPrinter#print(double[])
	 */
	@Override public void print(double[] values) {
		
		StringBuilder stringBuilder = null;
		
		for (int i = 20; i > 0; i--) {
			
			stringBuilder = new StringBuilder();
			
			for(double value : values) {
				
				if (this.printLine(value, i)) {
					
					stringBuilder.append("|");
					
				}
				else {
					
					stringBuilder.append(" ");
					
				} // if
				
			} // for
			
			Demo.println(stringBuilder.toString());
			
		} // for
		
	}
	
	/**
	 * Says, if a value is greater or less than the limit.
	 * 
	 * @param value the value.
	 * @param limit the limit.
	 * 
	 * @return <code>true</code>, if the value is greater or equal than the
	 * 		limit.<br/>
	 * 		<code>false</code>, otherwise.
	 */
	protected abstract boolean printLine(double value, int limit);

}
