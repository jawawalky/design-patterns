/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex01;


/**
 * The logarithmic printer represents a chart printer, which uses logarithmic
 * scales for the printing of the values.
 * 
 * @author Franz Tost
 *
 */
public class LogarithmicPrinter extends AbstractChartPrinter {

	// methods
	// ........................................................................
	
	/**
	 * Implements the <i>logarithmic</i> strategy.
	 * 
	 * @param value the value.
	 * @param limit the limit.
	 * 
	 * @return <code>true</code>, if <code>log10(value)</code> is greater or
	 * 		equal to the limit.<br/>
	 * 		<code>false</code>, if not.
	 * 
	 * @see demo.design.pattern.behavioral.ex01.AbstractChartPrinter#printLine(double, int)
	 */
	@Override protected boolean printLine(double value, int limit) {
		
		return (Math.log10(value) >= limit);
		
	}

}
