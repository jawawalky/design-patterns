/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex01;

/**
 * The {@link ChartPrinter} is the interface, which defines the
 * <i>strategy</i>. The implementations of the interface represent the
 * actual strategies.
 * 
 * @author Franz Tost
 *
 */
public interface ChartPrinter {
	
	// methods
	// ........................................................................
	
	/**
	 * Prints the values.
	 *   
	 * @param values the values.
	 */
	void print(double[] values);

}
