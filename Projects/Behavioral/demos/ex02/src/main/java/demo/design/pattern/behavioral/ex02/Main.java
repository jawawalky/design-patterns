/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex02;

import java.util.HashMap;
import java.util.Map;

import demo.util.Demo;

/**
 * This demo shows you, how you can use the <i>Strategy</i> pattern to
 * dynamically choose a certain calculation.
 * 
 * @author Franz Tost
 *
 */
public class Main {
	
	// constants /////
	
	private static final Map<String, Operation> OPERATIONS = new HashMap<>();
	
	static {
		
		OPERATIONS.put("+", (a, b) -> a + b);
		OPERATIONS.put("-", (a, b) -> a - b);
		OPERATIONS.put("*", (a, b) -> a * b);
		OPERATIONS.put("/", (a, b) -> a / b);
		
	}

	
	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final String symbol = Demo.input("+|-|*|/");
		
		final Operation op =  OPERATIONS.get(symbol);
		final double    a  = Demo.inputDouble("a", 0.0);
		final double    b  = Demo.inputDouble("b", 0.0);
		
		Demo.log(a + " " + symbol + " " + b + " = " + op.calculate(a, b));
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
