/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex11;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import demo.util.Demo;

/**
 * The calculator.
 * 
 * @author Franz Tost
 *
 */
public class Calculator {
	
	// inner classes
	// ........................................................................
	
	private class AddEdit extends AbstractUndoableEdit {
		
		// fields
		// ....................................................................
		
		private double value;
		
		// constructors
		// ....................................................................
		
		public AddEdit(double value) { this.value = value; }
		
		// methods
		// ....................................................................
		
		@Override public void redo() throws CannotRedoException {
			
			super.redo();
			Calculator.this.result += this.value;
			
		}

		@Override public void undo() throws CannotUndoException {
			
			super.undo();
			Calculator.this.result -= this.value;
			
		}

	}
	
	private class SubtractEdit extends AbstractUndoableEdit {
		
		// fields
		// ....................................................................
		
		private double value;
		
		// constructors
		// ....................................................................
		
		public SubtractEdit(double value) { this.value = value; }

		// methods
		// ....................................................................
		
		@Override public void redo() throws CannotRedoException {
			
			super.redo();
			Calculator.this.result -= this.value;
			
		}

		@Override public void undo() throws CannotUndoException {
			
			super.undo();
			Calculator.this.result += this.value;
			
		}

	}
	
	// fields
	// ........................................................................
	
	private UndoManager undoManager = new UndoManager();
	
	private double result;
	
	// methods
	// ........................................................................
	
	public void add(double value) {
		
		this.result += value;
		this.undoManager.addEdit(new AddEdit(value));
		
	}

	public void sub(double value) {
		
		this.result -= value;
		this.undoManager.addEdit(new SubtractEdit(value));
		
	}
	
	public void undo() {
		
		try {
			
			this.undoManager.undo();
			
		} // try
		catch (CannotUndoException e) {
			
			Demo.log("There is no action to be undone.");
			
		}
		
	}
	
	public void redo() {
		
		try {
			
			this.undoManager.redo();
			
		} // try
		catch (CannotRedoException e) {
			
			Demo.log("There is no action to be redone.");
			
		}
		
	}

	public double getResult() {
		
		return this.result;
		
	}
	
}
