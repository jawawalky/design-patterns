/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex72;

import demo.util.Demo;

/**
 * This demo shows you, how to implement a <i>Mediator</i> pattern.
 * 
 * @author Franz Tost
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 */
	private void runDemo() {
		
		Demo.log("Running demo ...");
		
		DemoFrame frame = new DemoFrame();
		frame.pack();
		frame.setVisible(true);
		
	}

	/**
	 * Runs the application.
	 * 
	 * @param args no arguments needed.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		Main demo = new Main();
		demo.runDemo();
		
	}
	
}