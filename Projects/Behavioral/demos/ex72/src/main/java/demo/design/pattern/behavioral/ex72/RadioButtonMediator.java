package demo.design.pattern.behavioral.ex72;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JRadioButton;

/**
 * A mediator for radio buttons, which belong together.
 * 
 * @author Franz Tost
 *
 */
public class RadioButtonMediator {
	
	// fields
	// ........................................................................
	
	private List<JRadioButton> radioButtons = new ArrayList<JRadioButton>();

	// methods
	// ........................................................................
	
	public void addRadioButton(final JRadioButton radioButton) {
		
		this.radioButtons.add(radioButton);
		radioButton.addActionListener(event -> onSelect(event));
		
	}
	
	private void onSelect(final ActionEvent event) {
		
		this.radioButtons.forEach(
			radioButton -> radioButton.setSelected(radioButton == event.getSource())
		);
		
	}
	
}
