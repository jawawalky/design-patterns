/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The template class.
 * 
 * @author Franz Tost
 *
 */
public abstract class Letter {
	
	// fields
	// ........................................................................
	
	private String addressee;
	
	private String sender;
	
	// methods
	// ........................................................................
	
	public String compose() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(sdf.format(new Date()));
		stringBuilder.append("\n");
		stringBuilder.append("------------------------------------------------------------");
		stringBuilder.append("\n\n");
		stringBuilder.append(this.getIntroduction());
		stringBuilder.append("\n\n\n");
		stringBuilder.append(this.getBody());
		stringBuilder.append("\n\n\n");
		stringBuilder.append(this.getEnding());
		stringBuilder.append("\n\n");
		stringBuilder.append("------------------------------------------------------------");
		stringBuilder.append("\n");
		stringBuilder.append("by Jukia Software");
		
		return stringBuilder.toString();
		
	}
	
	protected abstract String getIntroduction();

	protected abstract String getBody();

	protected abstract String getEnding();

	public String getAddressee() {
		
		return this.addressee;
		
	}

	public void setAddressee(String addressee) {
		
		this.addressee = addressee;
		
	}

	public String getSender() {
		
		return this.sender;
		
	}

	public void setSender(String sender) {
		
		this.sender = sender;
		
	}

}
