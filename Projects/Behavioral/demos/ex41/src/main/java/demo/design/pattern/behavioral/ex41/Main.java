/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;

import demo.util.Demo;

/**
 * This demo shows you, how to use template methods.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.println("A) Love Letter");
		Demo.println("==============");
		Demo.println();
		
		LoveLetter loveLetter = new LoveLetter();
		loveLetter.setAddressee("Jane");
		loveLetter.setSender("Tarzan");
		Demo.println(loveLetter.compose());
		
		Demo.println();
		Demo.println();

		
		Demo.println("B) Invoice");
		Demo.println("==========");
		Demo.println();
		
		Invoice invoice = new Invoice();
		invoice.setAddressee("Very Rich Company");
		invoice.setSender("Very Poor Company");
		invoice.setNumber(123456789);
		invoice.addItem("2x  Cup:      2.99      5,98");
		invoice.addItem("5x  Plate:    3.49     17.45");
		invoice.addItem("7x  Fork:     6.00     42.00");
		invoice.addItem("1x  Knife:    7.50      7.50");
		invoice.addItem("============================");
		invoice.addItem("    Total:             72.93");
		Demo.println(invoice.compose());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
