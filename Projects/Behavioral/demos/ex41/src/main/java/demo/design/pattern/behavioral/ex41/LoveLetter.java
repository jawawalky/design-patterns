/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;


public class LoveLetter extends Letter {
	
	// methods
	// ........................................................................

	@Override
	protected String getIntroduction() {
		
		return "Dear " + this.getAddressee() + ",";
		
	}

	@Override
	protected String getBody() {
		
		return "I love you.\n" +
			"I always did.\n" +
			"I always will.";
		
	}

	@Override
	protected String getEnding() {
		
		return "Truely yours " + this.getSender();
		
	}

}
