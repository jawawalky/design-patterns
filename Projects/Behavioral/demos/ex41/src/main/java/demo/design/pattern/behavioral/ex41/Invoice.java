/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;

import java.util.ArrayList;
import java.util.List;


public class Invoice extends Letter {
	
	// fields
	// ........................................................................
	
	private int number;
	
	private List<String> items = new ArrayList<String>();
	
	// methods
	// ........................................................................

	@Override
	protected String getIntroduction() {
		
		return "Invoice: " + this.getNumber() +
			"\n\n" +
			this.getAddressee();
		
	}

	@Override
	protected String getBody() {
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for (String item : this.items) {
			
			stringBuilder.append(item).append("\n");
			
		} // for
		
		return stringBuilder.toString();
		
	}

	@Override
	protected String getEnding() {
		
		return "Please pay within 2 weeks." +
			"\n\n" +
			this.getSender();
		
	}

	public int getNumber() {
		
		return this.number;
		
	}

	public void setNumber(int number) {
		
		this.number = number;
		
	}
	
	public void addItem(String item) {
		
		this.items.add(item);
		
	}

}
