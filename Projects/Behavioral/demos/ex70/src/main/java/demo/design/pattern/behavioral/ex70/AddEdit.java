/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex70;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

/**
 * An undoable calculation.
 * 
 * @author Franz Tost
 */
public class AddEdit extends AbstractUndoableEdit {
	
	// fields
	// ........................................................................
	
	private Calculator calculator;

	private int value;
	
	// constructors
	// ........................................................................

	public AddEdit(Calculator calculator, int value) {
		
		this.calculator = calculator;
		this.value = value;
		
	}
	
	// methods
	// ........................................................................

	public String getPresentationName() {
		
		return "add " + this.value;
		
	}

	public void redo() throws CannotRedoException {
		
		super.redo();
		this.calculator.add(this.value);
		
	}

	public void undo() throws CannotUndoException {
		
		super.undo();
		this.calculator.subtract(this.value);
		
	}

}