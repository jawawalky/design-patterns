/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex70;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import javax.swing.undo.UndoManager;

/**
 * The application with undo support.
 * 
 * @author Franz Tost
 */
public class Main {

	// fields
	// ........................................................................
	
	private UndoManager undoManager = new UndoManager();

	private Calculator calculator = new Calculator();
	
	// constructors
	// ........................................................................

	private Main() throws Exception {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(
				System.in));
		
		System.out.println("Commands: + <n> | - <n> | u | r | q");
		System.out.print("> ");
		String input = console.readLine();
		
		while (!"q".equals(input)) {
			
			this.performInput(input);
			System.out.print("> ");
			input = console.readLine();
			
		} // while
		
	}

	// methods
	// ........................................................................
	
	private void performInput(String input) {
		
		StringTokenizer t = new StringTokenizer(input, " \t");
		int n = t.countTokens();
		if (n == 2) {
			
			String command = t.nextToken();
			int value = 0;
			try {
				
				value = Integer.parseInt(t.nextToken());
				
			} // try
			catch (NumberFormatException e) {} // catch
			this.performCommand(command, value);
			
		}
		else if (n == 1) {
			
			String metaCommand = t.nextToken();
			this.performMetaCommand(metaCommand);
			
		}
		else {
			
			System.out.println("bad input");
			
		} // if

		System.out.println("value = " + this.calculator.getValue());
		
	}

	private void performCommand(String command, int value) {
		
		if ("+".equals(command)) {
			
			this.calculator.add(value);
			this.undoManager.addEdit(new AddEdit(this.calculator, value));
			
		}
		else if ("-".equals(command)) {
			
			this.calculator.subtract(value);
			this.undoManager.addEdit(new SubtractEdit(this.calculator, value));
			
		}
		else {
			
			System.out.println("bad input");
			
		} // if
		
	}

	private void performMetaCommand(String metaCommand) {
		
		if ("u".equals(metaCommand)) {
			
			if (this.undoManager.canUndo()) {
				
				System.out.println(this.undoManager.getUndoPresentationName());
				this.undoManager.undo();
				
			} // if
			
		}
		else if ("r".equals(metaCommand)) {
			
			if (this.undoManager.canRedo()) {
				
				System.out.println(this.undoManager.getRedoPresentationName());
				this.undoManager.redo();
				
			} // if
			
		}
		else {
			
			System.out.println("bad input");
			
		} // if
		
	}

	/**
	 * Runs the application.
	 * 
	 * @param args no arguments needed.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		new Main();
		
	}
	
}