/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex40;


/**
 * The template defines the basic design of the converter.
 * 
 * @author Franz Tost
 *
 */
public abstract class DocumentConverter {
	
	// fields
	// ........................................................................
	
	protected String inputLocation;
	
	protected String outputLocation;
	
	// constructors
	// ........................................................................
	
	public DocumentConverter(
		final String inputLocation,
		final String outputLocation
	) {
		
		this.inputLocation = inputLocation;
		this.outputLocation = outputLocation;
		
	}
	
	// methods
	// ........................................................................
	
	/**
	 * This is the template method, which will be called, when an input
	 * document should be converted. It defines the call order of
	 * abstract methods, which must be implemented in derived subclasses.
	 * In such way the template method defines the algorithm of the
	 * conversion process.
	 */
	public void convert() {
		
		// TODO
		//
		//  o Load the input document from its location.
		//
		//  o Convert the input document into the output document.
		//
		//  o Save the output document to its location.
		//
		
	}
	
	// TODO
	//
	//  o Define the three abstract methods
	//
	//      o 'protected abstract Document loadDocument()'
	//      o 'protected abstract Document convertDocument(Document inputDocument)'
	//      o 'protected abstract void saveDocument(Document outputDocument)'
	//

}
