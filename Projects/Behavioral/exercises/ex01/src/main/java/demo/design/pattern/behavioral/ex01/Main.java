/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex01;

import demo.util.Demo;

/**
 * This demo shows you, how you can use different strategies to print
 * a list of <code>double</code> values. We use the <i>Strategy</i> pattern
 * to solve the task.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constants
	// ........................................................................
	
	/**
	 * A list of values, which do not differ too much. Printing a linear
	 * chart is quite good. Using a logarithmic chart does not show good
	 * results.
	 */
	private static final double[] VALUES_1 = {
		5.0, 3.0, 8.0, 13.0, 18.0, 9.0, 10.0, 10.0, 7.0, 19.0,
		20.0, 20.0, 16.0, 14.0, 15.0, 12.0, 8.0, 9.0, 11.0, 14.0 
	};

	/**
	 * A list of values, which differ a lot. Printing a linear
	 * chart is quite bad. Using a logarithmic chart does show good
	 * results.
	 */
	private static final double[] VALUES_2 = {
		5.0, 10.0, 800.0, 1300.0, 18000.0, 900000.0, 10000.0, 100000000.0, 700000000000.0, 19000000.0,
		2000000.0, 20000.0, 160000000000.0, 140000000000.0, 1500000.0, 120000.0, 8000000000000000000.0, 90000000000.0, 1100000.0, 1400.0 
	};

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create a 'LinearPrinter' and a 'LogarithmicPrinter'.
		//
		//  o Print 'VALUES_1' and 'VALUES_2' with the 'LinearPrinter' using
		//    the method 'print(ChartPrinter, double[])'.
		//
		//  o Do the same with the 'LogarithmicPrinter'.
		//
		
		Demo.log("Finished.");

	}
	
	// TODO
	//
	//  o Create a method called
	//    
	//      'private void print(ChartPrinter strategy, double[] values)'
	//
	//    which uses the given strategy to print the specified values.
	//
		
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
