/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex70;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * The application with undo support.
 * 
 * @author Franz Tost
 */
public class Main {
	
	// fields
	// ........................................................................
	
	//
	// TODO
	//
	// o Create an attribute 'undoManager' of type 'UndoManager'.

	private Calculator calculator = new Calculator();

	// constructor
	// ........................................................................
	
	private Main() throws Exception {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(
				System.in));
		System.out
				.println("Commands: add <nnn> | subtract <nnn> | undo | redo | end");
		System.out.print("> ");
		String input = console.readLine();
		
		while (!"end".equals(input)) {
			
			this.performInput(input);
			System.out.print("> ");
			input = console.readLine();
			
		} // while
		
	}

	private void performInput(String input) {
		
		StringTokenizer t = new StringTokenizer(input, " \t");
		int n = t.countTokens();
		if (n == 2) {
			
			String command = t.nextToken();
			int value = 0;
			try {
				
				value = Integer.parseInt(t.nextToken());
				
			} // try
			catch (NumberFormatException e) {} // catch
			this.performCommand(command, value);
			
		}
		else if (n == 1) {
			
			String metaCommand = t.nextToken();
			this.performMetaCommand(metaCommand);
			
		}
		else {
			
			System.out.println("bad input");
			
		} // if

		System.out.println("value = " + this.calculator.getValue());
		
	}

	private void performCommand(String command, int value) {
		
		// TODO
		//
		//  o If the command is 'add', do the following
		//
		//      o Call the 'add' method of the calculator.
		//
		//      o Create an 'AddEdit' with the relevant values.
		//
		//      o Add the edit object to the undo manager.
		//
		//  o If the command is 'subtract', do the following
		//
		//      o Call the 'subtract' method of the calculator.
		//
		//      o Create an 'SubtractEdit' with the relevant values.
		//
		//      o Add the edit object to the undo manager.
		
	}

	private void performMetaCommand(String metaCommand) {
		
		// TODO
		//
		//  o If the command is 'undo', call the 'undo()' method on the
		//    undo manager.
		//
		//  o If the command is 'redo', call the 'redo()' method on the
		//    undo manager.
		
	}
	
	/**
	 * Runs the application.
	 * 
	 * @param args no arguments needed.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		new Main();
		
	}

}