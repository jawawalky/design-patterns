/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2008 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex70;

/**
 * A calculator.
 * 
 * @author Franz Tost
 */
public class Calculator {
	
	// fields
	// ........................................................................

	private int value;

	// methods
	// ........................................................................

	public void add(int value) {
		
		this.value += value;
		
	}

	public void subtract(int value) {
		
		this.value -= value;
		
	}

	public int getValue() {
		
		return this.value;
		
	}
	
}