/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

/**
 * A remote control, which allows us to turn a TV set on and off and which
 * let's us select a channel.
 * 
 * @author Franz Tost
 *
 */
public class RemoteControl {
	
	// fields /////
	
	// TODO
	//
	//  o Add fields for each possible command
	//
	//      o ON
	//      o OFF
	//      o Channel 1 - 5
	//
	
	
	// constructors /////
	
	public RemoteControl(final TV tv) {
		
		super();
		
		// TODO
		//
		//  o Use the TV to define the appropriate commands and
		//    assign them to their corresponding fields.
		//
		//    Hint: Use lambda expressions.
		//
		
	}
	
	
	// methods /////
	
	// TODO
	//
	//  o Write simple methods like 'pressOn()', 'pressChn1()' etc.,
	//    which perform the according command.
	//
	//    Hint: You can use the method 'execute(Command)', if you want.
	//
	
	private void execute(final Command command) {
		
		// TODO
		//
		//  o Execute the specified command.
		//
		
	}
	
}
