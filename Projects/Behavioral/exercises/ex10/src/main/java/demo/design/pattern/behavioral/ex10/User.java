/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex10;

/**
 * A user operating a TV set by a remote control.
 * 
 * @author Franz Tost
 *
 */
public class User {
	
	// fields /////
	
	private RemoteControl remoteControl;

	
	// methods /////
	
	// TODO
	//
	//  o Write a method 'startTV(...)', which simulates the user taking
	//    the remote control and turning the TV on.
	//    What parameter has to be passed to the method?
	//
	
	// TODO
	//
	//  o Write a method 'stopTV()', which turns the TV off.
	//
	
	// TODO
	//
	//  o Write a method 'selectChannel(int)', which allows the user to
	//    choose the channel he/she wants to watch.
	//
	
}
