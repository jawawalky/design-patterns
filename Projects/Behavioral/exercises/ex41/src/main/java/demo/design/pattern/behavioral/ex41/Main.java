/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;

import demo.util.Demo;

/**
 * This demo shows you, how to use template methods.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		
		Demo.println("A) Love Letter");
		Demo.println("==============");
		Demo.println();
		
		// TODO
		//
		//  o Create a love letter.
		//
		//  o Set the required attributes.
		//
		//  o Compose the letter an print it on the console.
		
		Demo.println();
		Demo.println();

		
		Demo.println("B) Invoice");
		Demo.println("==========");
		Demo.println();
		
		// TODO
		//
		//  o Create an invoice.
		//
		//  o Set the required attributes.
		//
		//  o Compose the invoice an print it on the console.
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
