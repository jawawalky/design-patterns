/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex41;

import java.text.SimpleDateFormat;

/**
 * The template class.
 * 
 * @author Franz Tost
 *
 */
public abstract class Letter {
	
	// fields
	// ........................................................................
	
	private String addressee;
	
	private String sender;
	
	// methods
	// ........................................................................
	
	public String compose() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		StringBuilder stringBuilder = new StringBuilder();
		
		// TODO
		//
		//  o Design the layout and content parts of your letter.
		//
		//  o Contents, which should be variable, must be provided by
		//    abstract methods, which are overridden in sub-classes.
		
		return stringBuilder.toString();
		
	}
	
	// TODO
	//
	//  o Design abstract methods for your variable content, which you call
	//    in your template method 'compose()'.
	//
	//    Suggestion:
	//
	//      o protected abstract String getIntroduction();
	//      o protected abstract String getBody();
	//      o protected abstract String getEnding();

	public String getAddressee() {
		
		return this.addressee;
		
	}

	public void setAddressee(String addressee) {
		
		this.addressee = addressee;
		
	}

	public String getSender() {
		
		return this.sender;
		
	}

	public void setSender(String sender) {
		
		this.sender = sender;
		
	}

}
