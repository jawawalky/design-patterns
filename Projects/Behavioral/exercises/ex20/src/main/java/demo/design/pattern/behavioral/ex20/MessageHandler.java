/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20;

/**
 * The interface used for the design of the message handler chain.
 * 
 * @author Franz Tost
 *
 */
public interface MessageHandler {
	
	// methods
	// ........................................................................
	
	/**
	 * Handles the incoming message.
	 * 
	 * @param message the message.
	 */
	void handleMessage(Message message);
	
	/**
	 * Sets the handler, which comes after this handler.
	 * 
	 * @param handler the next message handler.
	 */
	void setNextHandler(MessageHandler handler);

}
