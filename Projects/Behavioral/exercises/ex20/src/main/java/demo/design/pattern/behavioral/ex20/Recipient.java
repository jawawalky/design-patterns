/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20;

import demo.util.Demo;

/**
 * A recipient can receive messages.
 * 
 * @author Franz Tost
 *
 */
public class Recipient {
	
	// fields
	// ........................................................................
	
	private String name;
	
	// constructors
	// ........................................................................
	
	public Recipient(String name) {
		
		super();
		
		this.name = name;
		
	}
	
	// methods
	// ........................................................................
	
	public String getName() {
		
		return this.name;
		
	}
	
	/**
	 * Prints a message on the console, when he receives one.
	 * 
	 * @param message the message.
	 */
	public void receive(Message message) {
		
		Demo.log(this.name + " has received the following message:");
		Demo.log("  [" + message.getPriority() + "]: " + new String(message.getContent()));
		
	}
	
}
