/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex20;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import demo.util.Demo;

/**
 * A message with the following information
 * <ul>
 * 	<li>the recipient</li>
 * 	<li>the priority</li>
 * 	<li>the message content</li>
 * </ul>
 * When the message is created, we calculate a message digest for the message.
 * It can be used to verify its integrity. (Not really, since the data is
 * not really protected!)
 * 
 * @author Franz Tost
 *
 */
public class Message {
	
	// enums
	// ........................................................................
	
	public static enum Priority {
		
		LOW,
		NORMAL,
		HIGH
		
	}

	// fields
	// ........................................................................
	
	private String recipientName;

	private Priority priority = Priority.NORMAL;
	
	private byte[] digest;
	
	private byte[] content;
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates a message, which can be sent to a certain recipient. We
	 * calculate a message digest for the content of the message.
	 * 
	 * @param recipientName the name of the recipient.
	 * @param priority the priority of the message.
	 * @param content the content of the message.
	 */
	public Message(String recipientName, Priority priority, byte[] content) {
		
		super();
		
		this.recipientName = recipientName;
		this.priority = priority;
		this.content = content;
		
		try {
			
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			this.digest = messageDigest.digest(content);
			
		} // try
		catch (NoSuchAlgorithmException e) {
			
			Demo.terminate(e, 1);
			
		} // catch
		
	}

	public byte[] getContent() {
		
		return this.content;
		
	}
	
	public byte[] getDigest() {
		
		return this.digest;
		
	}

	public Priority getPriority() {
		
		return this.priority;
		
	}

	public String getRecipientName() {
		
		return this.recipientName;
		
	}

	/**
	 * Not really necessary. We use it just for setting the message content a
	 * second time, so we can simulate a manupulation of the message.
	 * 
	 * @param content the new message content.
	 */
	public void setContent(byte[] content) {
		
		this.content = content;
		
	}

	@Override
	public String toString() {
		
		return "Message for " + this.getRecipientName() + " with priority " + this.getPriority() + ".";
		
	}

}
