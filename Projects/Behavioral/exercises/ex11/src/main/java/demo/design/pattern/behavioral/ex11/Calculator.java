/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex11;


/**
 * The calculator.
 * 
 * @author Franz Tost
 *
 */
public class Calculator {
	
	// inner classes
	// ........................................................................
	
	// TODO
	//
	//  o Create a private inner class named 'AddEdit', which is derived from
	//    the base class 'AbstractUndoableEdit'.
	//
	//  o Add required attributes and constructors. Consider what kind of
	//    information you need, if you want to undo or redo the addition.
	//
	//  o Override the method 'undo()', inherited from the super class.
	//    Call 'super.undo()' first and then implement your code, which undoes
	//    the addition.
	//
	//  o Override the method 'redo()', inherited from the super class.
	//    Call 'super.redo()' first and then implement your code, which redoes
	//    the addition.
	//
	
	// TODO
	//
	//  o Create a private inner class named 'SubtractEdit', which is derived
	//    from the base class 'AbstractUndoableEdit'.
	//
	//  o Add required attributes and constructors. Consider what kind of
	//    information you need, if you want to undo or redo the subtraction.
	//
	//  o Override the method 'undo()', inherited from the super class.
	//    Call 'super.undo()' first and then implement your code, which undoes
	//    the subtraction.
	//
	//  o Override the method 'redo()', inherited from the super class.
	//    Call 'super.redo()' first and then implement your code, which redoes
	//    the subtraction.
	//
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add an instance of the 'UndoManager' and call it 'undoManager'.
	//
	
	private double result;
	
	// methods
	// ........................................................................
	
	public void add(double value) {
		
		this.result += value;
		
		// TODO
		//
		//  o Register an 'AddEdit' object with the adequate value.
		//
		
	}

	public void sub(double value) {
		
		this.result -= value;
		
		// TODO
		//
		//  o Register a 'SubtractEdit' object with the adequate value.
		//
		
	}
	
	public void undo() {
		
		// TODO
		//
		//  o Let the undoManager undo the last action.
		//
		//  o Catch the 'CannotUndoException' and issue a message on the console.
		//
		
	}
	
	public void redo() {
		
		// TODO
		//
		//  o Let the undoManager redo the last undone action.
		//
		//  o Catch the 'CannotRedoException' and issue a message on the console.
		//
		
	}

	public double getResult() {
		
		return this.result;
		
	}
	
}
