/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex11;

import demo.util.Demo;

/**
 * This demo shows you, how the undo and redo mechanism works.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() {}

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Calculator calculator = new Calculator();
		String operation = null;
		
		while (true) {
			
			operation = Demo.input("Choose operation [+|-|u|r|q]:");
			
			if ("q".equals(operation)) {
				
				break;
				
			}
			else if ("u".equals(operation)) {
				
				calculator.undo();
				
			}
			else if ("r".equals(operation)) {
				
				calculator.redo();
				
			}
			else if ("+".equals(operation)) {
				
				double value = Demo.inputDouble("x", 0.0);
				calculator.add(value);
				
			}
			else if ("-".equals(operation)) {
				
				double value = Demo.inputDouble("x", 0.0);
				calculator.sub(value);
				
			} // if
			
			Demo.log("y = " + calculator.getResult());
			
		} // while
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
