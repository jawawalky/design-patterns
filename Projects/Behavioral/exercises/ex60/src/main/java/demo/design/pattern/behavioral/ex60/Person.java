/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;


/**
 * A person.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Optionally implement the interface 'Visitable'.
 */
public class Person {

	// fields
	// ........................................................................
	
	private String name;
	
	private Heart heart = new Heart();
	
	private Lung  lung  = new Lung();
	
	private Blood blood = new Blood();
	
	// constructors
	// ........................................................................
	
	public Person(String name) {
		
		super();
		
		this.name = name;
		
	}
	
	// methods
	// ........................................................................
	
	// TODO
	// 
	//  o Implement the 'void accept(Visitor)' method, which accepts a visitor
	//    to this class.
	
	public void sleep() {
		
		this.heart.setPulse(70);
		this.lung.setRespiration(15);
		this.blood.setAlcohol(0.0);
		this.blood.setPressure(80.0);
		
	}
	
	public void drink() {
		
		this.blood.setAlcohol(this.blood.getAlcohol() + 0.3);
		
	}
	
	// TODO
	//
	//  o Replace the method 'eat()' by a visitor class.
	//
	public void eat() {
		
		this.heart.setPulse(120);
		this.lung.setRespiration(50);
		this.blood.setAlcohol(0.0);
		this.blood.setPressure(this.blood.getPressure() + 10);
		
	}
	
	public void smoke() {
		
		this.heart.setPulse(70);
		this.lung.setRespiration(10);
		this.blood.setAlcohol(0.0);
		this.blood.setPressure(100.0);
		
	}
	
	public void run() {
		
		this.heart.setPulse(Math.max(150, this.heart.getPulse() + 10));
		this.lung.setRespiration(Math.max(60, this.lung.getRespiration() + 5));
		
	}

	public String getName() {
		
		return this.name;
		
	}

}
