/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Optionally implement the interface 'Visitable'.
 */
public class Heart {

	// fields
	// ........................................................................
	
	private int pulse = 70;
	
	// methods
	// ........................................................................
	
	// TODO
	// 
	//  o Implement the 'void accept(Visitor)' method, which accepts a visitor
	//    to this class.

	public int  getPulse()          { return this.pulse;  }
	public void setPulse(int pulse) { this.pulse = pulse; }
	
}
