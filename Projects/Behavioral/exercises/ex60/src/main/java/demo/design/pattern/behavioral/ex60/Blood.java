/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2011 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex60;

/**
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Optionally implement the interface 'Visitable'.
 */
public class Blood {

	// fields
	// ........................................................................
	
	private double pressure = 80.0;
	
	private double alcohol = 0.0;
	
	// methods
	// ........................................................................
	
	// TODO
	// 
	//  o Implement the 'void accept(Visitor)' method, which accepts a visitor
	//    to this class.

	public double getPressure()                { return this.pressure;     }
	public void   setPressure(double pressure) { this.pressure = pressure; }

	public double getAlcohol()                 { return this.alcohol;      }
	public void   setAlcohol(double alcohol)   { this.alcohol = alcohol;   }
	
}
