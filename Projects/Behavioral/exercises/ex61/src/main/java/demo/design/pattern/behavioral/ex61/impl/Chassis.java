/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

/**
 * The chassis.
 * 
 * @author Franz Tost
 *
 */
public class Chassis extends AbstractPart {

	// constructors
	// ........................................................................

	public Chassis(String name, double price, double weight) {
		
		super(name, price, weight);
		
	}

}
