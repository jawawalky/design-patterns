/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61;

/**
 * The interface for item, which are a part of something bigger.
 * 
 * @author Franz Tost
 *
 */
public interface Part {
	
	// methods
	// ........................................................................
	
	String getName();
	
	double getPrice();
	
	void setPrice(double price);
	
	double getWeight();
	
	// TODO
	//
	//  o Add a method, which accepts a visitor on the part, e.g.
	//
	//      'void accept(PartVisitor visitor)'
	//
	
}
