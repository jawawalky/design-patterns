/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

/**
 * An airbag.
 * 
 * @author Franz Tost
 *
 */
public class Airbag extends AbstractPart {
	
	// constructors
	// ........................................................................

	public Airbag(String name) {
		
		super(name, 51.30, 0.520);
		
	}

}
