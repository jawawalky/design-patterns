/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * A car is an object, which is composed of many parts.
 * 
 * @author Franz Tost
 *
 */
public class Car {
	
	// fields
	// ........................................................................

	private String number;
	
	private Chassis chassis;
	
	private List<Airbag> airbags = new ArrayList<Airbag>();
	
	private List<Wheel> wheels = new ArrayList<Wheel>();
	
	private List<Door> doors = new ArrayList<Door>();
	
	// constructors
	// ........................................................................

	public Car(String number) {
		
		this.number = number;
		
	}
	
	// methods
	// ........................................................................

	public String getNumber() {
		
		return this.number;
		
	}
	
	public void setChassis(Chassis chassis) {
		
		this.chassis = chassis;
		
	}
	
	public void addAirbag(Airbag airbag) {
		
		this.airbags.add(airbag);
		
	}

	public void addWheel(Wheel wheel) {
		
		this.wheels.add(wheel);
		
	}

	public void addDoor(Door door) {
		
		this.doors.add(door);
		
	}
	
	/**
	 * Calculates the price of the car. We use the {@link PriceFinder} to
	 * determine the total price of the whole car. The {@link PriceFinder}
	 * implements the {@link PartVisitor}.
	 * 
	 * @return The price.
	 */
	public double getPrice() {
		
		// TODO
		//
		//  o Use the 'PriceFinder' to determine the total price of the car.
		//
		
		return 0.0;
		
	}
	
	/**
	 * Calculates the weight of the car. We use the {@link WeightFinder} to
	 * determine the total weight of the whole car. The {@link WeightFinder}
	 * implements the {@link PartVisitor}.
	 * 
	 * @return The weight.
	 */
	public double getWeight() {
		
		// TODO
		//
		//  o Use the 'WeightFinder' to determine the total price of the car.
		//
		
		return 0.0;
		
	}
	
	/**
	 * Returns the number of parts of the car. We use the {@link PartCounter}
	 * to determine the total weight of the whole car. The
	 * {@link PartCounter} implements the {@link PartVisitor}.
	 * 
	 * @return The part count.
	 */
	public int getPartCount() {
		
		// TODO
		//
		//  o Use the 'PartCounter' to determine the total number of parts
		//    of the car.
		//
		
		return 0;
		
	}
	
	/**
	 * Prints a list of all parts of the car. We use the {@link PartList} to
	 * print all parts. The {@link PartList} implements the {@link PartVisitor}.
	 */
	public void getPartList() {
		
		// TODO
		//
		//  o Use the 'PartList' to print all parts of the car on the console.
		//
		
	}
	
	// TODO
	//
	//  o Implement a private method 'accept(PartVisitor)', which lets the
	//    visitor visit all part of the car.
	//
	
}
