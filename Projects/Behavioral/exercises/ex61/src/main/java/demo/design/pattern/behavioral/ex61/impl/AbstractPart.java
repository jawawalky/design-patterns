/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex61.impl;

import demo.design.pattern.behavioral.ex61.Part;

/**
 * A base class for parts.
 * 
 * @author Franz Tost
 *
 */
public class AbstractPart implements Part {
	
	// fields
	// ........................................................................
	
	private String name;
	
	private double price;
	
	private double weight;
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates the part.
	 * 
	 * @param name the part name.
	 * @param price the price of the part.
	 * @param weight the weight of the part.
	 */
	public AbstractPart(String name, double price, double weight) {
		
		this.name = name;
		this.price = price;
		this.weight = weight;
		
	}

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the 'accept(PartVisitor)' method now defined in the
	//    'Part' interface. Let the visitor visit this part.
	//

	public String getName() {
		
		return this.name;
		
	}

	public double getPrice() {
		
		return this.price;
		
	}
	
	public void setPrice(double price) {
		
		this.price = price;
		
	}

	public double getWeight() {
		
		return this.weight;
		
	}

	@Override
	public String toString() {
		
		return this.getName() + " (p = " + this.getPrice() + ", w = " + this.getWeight() + ")";
		
	}

}
