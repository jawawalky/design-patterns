/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex50;

import demo.util.Demo;

/**
 * This demo shows you, how the <i>Observer</i> pattern works.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create an instance of the class 'Logger'.
		//
		//  o Register a 'ConsoleAppender' and a 'FileAppender' with the
		//    logger.
		//
		//  o Log some message.
		//
		//  o Unregister the 'FileAppender'.
		//
		//  o Log some message.
		//
		//  o Unregister the 'ConsoleAppender' and register the 'FileAppender'.
		//
		//  o Log some message.
		//
		//  o Register the 'ConsoleAppender'.
		//
		//  o Log some message.
		//
		//  o View the output on the console and in the log file.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
