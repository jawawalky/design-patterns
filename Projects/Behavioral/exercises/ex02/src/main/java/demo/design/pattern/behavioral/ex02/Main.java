/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.behavioral.ex02;

import demo.util.Demo;

/**
 * This demo shows you, how you can use the <i>Strategy</i> pattern to
 * dynamically choose a certain calculation.
 * 
 * @author Franz Tost
 *
 */
public class Main {
	
	// constants /////
	
	// TODO
	//
	//  o Create a static map, which allows you to associate a symbol,
	//    such as '+', with its corresponding arithmetic operation.
	//
	//  o Use a static block to store arithmetic operations for
	//    '+', '-', '*' and '/'.
	//
	//    Hint: Use lambda expressions for the definition of the operations.
	//
	
	
	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final String symbol = Demo.input("+|-|*|/");
		
		// TODO
		//
		//  o Find the operation, which belongs to the specified symbol.
		//
		
		final double    a  = Demo.inputDouble("a", 0.0);
		final double    b  = Demo.inputDouble("b", 0.0);

		// TODO
		//
		//  o Perform the operation and print the result on the console.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
