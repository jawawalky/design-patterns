The Mediator Pattern
====================

  A mediator pattern mediates between several objects.
  

A) Implementing the Mediator
============================

  1) Create a class 'demo.behavioral.patterns.ex75.RadioButtonMediator'.
  
  2) Add a list of 'JRadioButtons'.
  
  3) Add a method 'addRadioButton(JRadioButton)', which adds the radio button
     to the mediator.
     
  4) When the radio button is added to the mediator, it should register
     an action listener. When a radio button is selected, then this radio
     button should be selected, while the other radio buttons should be
     deselected.

     
B) Using the Mediator
=====================

  'DemoFrame':
  ------------
  
  1) Create an instance of the radio button mediator.
  
  2) Add all radio buttons to the mediator.
