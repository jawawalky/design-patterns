package demo.design.pattern.behavioral.ex72;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JRadioButton;

/**
 * 
 * @author Franz Tost
 *
 */
public class DemoFrame extends JFrame {
	
	// fields
	// ........................................................................
	
	private JRadioButton rbA = new JRadioButton("A");

	private JRadioButton rbB = new JRadioButton("B");

	private JRadioButton rbC = new JRadioButton("C");

	private JRadioButton rbD = new JRadioButton("D");

	// constructors
	// ........................................................................
	
	public DemoFrame() {
		
		super("Behavioral Patterns (75), Mediator Demo");
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new GridLayout(4, 1, 3, 3));
		this.add(this.rbA);
		this.add(this.rbB);
		this.add(this.rbC);
		this.add(this.rbD);
		
		// TODO
		//
		//  o Create an instance of the radio button mediator.
		//
		//  o Add all radio buttons to the mediator.
		
	}
	
}
