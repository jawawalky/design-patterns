/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex21.impl;

import demo.design.pattern.creational.ex21.Car;

/**
 * A BMW X5.
 * 
 * @author Franz Tost
 *
 */
public class X5 implements Car {
	
	// methods
	// ........................................................................

	/* (non-Javadoc)
	 * @see demo.design.ex01.Car#getModel()
	 */
	public String getModel() {
		
		return "X5";
		
	}

	/* (non-Javadoc)
	 * @see demo.design.ex01.Car#getType()
	 */
	public Type getType() {
		
		return Car.Type.SUV;
		
	}

	@Override
	public String toString() {
		
		return this.getModel() + " (" + this.getType() + ")";
		
	}
	
}
