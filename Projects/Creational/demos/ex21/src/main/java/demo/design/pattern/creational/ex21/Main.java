/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex21;

import demo.design.pattern.creational.ex21.impl.BMWFactory;
import demo.util.Demo;

/**
 * This demo shows you, how the <i>Factory</i> design pattern adds flexibility
 * to the creation process of your objects. You can use parameters to control
 * what kind of objects will be created.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		CarFactory carFactory = this.createFactory();
		
		Demo.log("Creating cars:");
		Demo.log("  1. " + carFactory.create("X5"));
		Demo.log("  2. " + carFactory.create("Z4"));
		Demo.log("  3. " + carFactory.create("Q9"));
		Demo.log("  4. " + carFactory.create("Z4"));

		Demo.log("Finished.");

	}
	
	/**
	 * Creates and returns the factory object, which will be used for the
	 * creation of the cars.
	 * 
	 * @return The factory.
	 */
	private CarFactory createFactory() {
		
		return new BMWFactory();
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
