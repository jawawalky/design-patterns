/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex21.impl;

import java.util.HashMap;
import java.util.Map;

import demo.design.pattern.creational.ex21.Car;
import demo.design.pattern.creational.ex21.CarFactory;
import demo.util.Demo;

/**
 * The fatory, which produces {@link X5} and {@link Z4} cars.
 * 
 * @author Franz Tost
 *
 */
public class BMWFactory implements CarFactory {
	
	// fields
	// ........................................................................
	
	/**
	 * The models produced by this factory.
	 */
	private Map<String, Class<?>> models = new HashMap<String, Class<?>>();
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates the factory, which can produce
	 * <ul>
	 * 	<li><i>X5</i> and</li>
	 * 	<li><i>Z4</i></li>
	 * </ul>
	 * cars.
	 */
	public BMWFactory() {
		
		this.models.put("X5", X5.class);
		this.models.put("Z4", Z4.class);
		
	}

	// methods
	// ........................................................................

	public Car create(String model) {
		
		Class<?> modelClass = this.models.get(model);
		
		if (modelClass == null) {
			
			return null;
			
		} // if
		
		try {
			
			return (Car) modelClass.newInstance();
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		} // catch
		
	}

}
