/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex22.impl;

import demo.design.pattern.creational.ex22.Calculator;
import demo.design.pattern.creational.ex22.CalculatorFactory;
import demo.util.Demo;

/**
 * A {@link CalculatorFactory}, which uses reflection for the creation of
 * its objects.
 * 
 * @author Franz Tost
 *
 */
public class ReflectiveFactory implements CalculatorFactory {
	
	// methods
	// ........................................................................

	/**
	 * Uses reflection to create the object. This is a very flexible method
	 * for object creation, but also a very expensive operation in terms of
	 * performance.
	 * 
	 * @return The {@link Calculator} object.
	 * 
	 * @see demo.design.pattern.creational.ex22.CalculatorFactory#create()
	 */
	public Calculator create() {
		
		try {
			
			Class<?> calculatorClass =
				Class.forName("demo.design.pattern.creational.ex22.impl.CalculatorImpl");
			return (Calculator) calculatorClass.newInstance();
			
		}
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		}
		
	}

}
