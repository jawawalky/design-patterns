/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex22.impl;

import demo.design.pattern.creational.ex22.Calculator;
import demo.design.pattern.creational.ex22.CalculatorFactory;

/**
 * A {@link CalculatorFactory}, which always returns the same object.
 * 
 * @author Franz Tost
 *
 */
public class SingletonFactory implements CalculatorFactory {
	
	// fields
	// ........................................................................
	
	/**
	 * The only {@link Calculator} instance.
	 */
	private Calculator calculator = new CalculatorImpl();

	// methods
	// ........................................................................

	/**
	 * Always returns the same instance. This method uses the least memory
	 * and is the fastest. Though threading problems should be considered
	 * here.
	 * 
	 * @return The {@link Calculator} instance.
	 * 
	 * @see demo.design.pattern.creational.ex22.CalculatorFactory#create()
	 */
	public Calculator create() {
		
		return this.calculator;
		
	}

}
