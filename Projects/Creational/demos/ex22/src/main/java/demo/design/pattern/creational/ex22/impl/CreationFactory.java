/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex22.impl;

import demo.design.pattern.creational.ex22.Calculator;
import demo.design.pattern.creational.ex22.CalculatorFactory;

/**
 * A {@link CalculatorFactory}, which creates its objects with the
 * <code>new</code> operator.
 * 
 * @author Franz Tost
 *
 */
public class CreationFactory implements CalculatorFactory {
	
	// methods
	// ........................................................................

	/**
	 * Creates objects by using the <code>new</code> operator. This allows
	 * each user to have his own object. Though when many instances are being
	 * created this may affect performance.
	 * 
	 * @return The {@link Calculator} object.
	 * 
	 * @see demo.design.pattern.creational.ex22.CalculatorFactory#create()
	 */
	public Calculator create() {
		
		return new CalculatorImpl();
		
	}

}
