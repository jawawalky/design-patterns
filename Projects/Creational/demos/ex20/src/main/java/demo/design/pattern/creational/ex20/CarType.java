/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20;

/**
 * The enum for the different car types.
 * 
 * @author Franz Tost
 *
 */
public enum CarType {
	
	// enums
	// ........................................................................
	
	LIMOUSINE("Limousine"),
	CABRIOLET("Cabriolet"),
	COMBI("Combi"),
	VAN("Van"),
	OFFROADER("Off-Roader"),
	SUV("Sports Utility Vehicle");
	
	// fields
	// ........................................................................
	
	private String name;
	
	// constructors
	// ........................................................................
	
	private CarType(final String name) { this.name = name; }

	// methods
	// ........................................................................
	
	@Override public String toString() { return this.name; }
	
}
