/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20.impl;

import demo.design.pattern.creational.ex20.Car;
import demo.design.pattern.creational.ex20.CarType;

/**
 * A BMW X5.
 * 
 * @author Franz Tost
 *
 */
public class X5 implements Car {
	
	// methods
	// ........................................................................

	public String getModel() { return "X5"; }

	public CarType getType() { return CarType.SUV; }

	@Override public String toString() {
		
		return this.getModel() + " (" + this.getType() + ")";
		
	}
	
}
