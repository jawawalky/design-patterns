/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20.impl;

import demo.design.pattern.creational.ex20.Car;
import demo.design.pattern.creational.ex20.CarFactory;

/**
 * The factory, which produces {@link A2} cars.
 * 
 * @author Franz Tost
 *
 */
public class A2Factory implements CarFactory {
	
	// methods
	// ........................................................................

	public Car create() { return new A2(); }

}
