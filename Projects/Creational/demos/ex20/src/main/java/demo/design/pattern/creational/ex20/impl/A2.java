/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20.impl;

import demo.design.pattern.creational.ex20.Car;
import demo.design.pattern.creational.ex20.CarType;

/**
 * An Audi A2.
 * 
 * @author Franz Tost
 *
 */
public class A2 implements Car {
	
	// methods
	// ........................................................................

	public String getModel() { return "A2"; }

	public CarType getType() { return CarType.COMBI; }

	@Override public String toString() {
		
		return this.getModel() + " (" + this.getType() + ")";
		
	}
	
}
