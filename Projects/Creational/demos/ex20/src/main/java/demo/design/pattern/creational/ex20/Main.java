/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20;

import demo.util.Demo;

/**
 * This demo shows you, how the <i>Factory</i> design pattern adds flexibility
 * to your application. What kind of objects will be created, can be configured
 * outside your program.<br/>
 * Furthermore you can easily add new factory implementations without changing
 * the code of your application.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		CarFactory carFactory = this.createFactory();
		
		Demo.log("Creating cars:");
		Demo.log("  1. " + carFactory.create());
		Demo.log("  2. " + carFactory.create());
		Demo.log("  3. " + carFactory.create());

		Demo.log("Finished.");

	}
	
	private CarFactory createFactory() {
		
		try {
			
			String factoryClassName = Demo.getProperty("car.factory");
			
			Demo.log("Factory class: " + factoryClassName);
			
			Class<?> factoryClass = Class.forName(factoryClassName);
			return (CarFactory) factoryClass.getConstructor().newInstance();
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		} // catch
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
