/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex01;

import java.io.FileReader;
import java.util.Properties;

import demo.util.Demo;

/**
 * A singleton which allows us to access the application parameters stored
 * in the file <code>conf/application.properties</code>.
 * 
 * @author Franz Tost
 *
 */
public class ApplicationParameters {
	
	// fields
	// ........................................................................
	
	/**
	 * The one and only instance of this, which will be available through out
	 * the application.
	 */
	private static ApplicationParameters instance;
	
	/**
	 * The vendor name;
	 */
	private String vendor;
	
	/**
	 * The version number;
	 */
	private String version;
	
	/**
	 * The build number;
	 */
	private String build;
	
	/**
	 * The application name;
	 */
	private String name;
	
	// constructors
	// ........................................................................
	
	/**
	 * A private default constructor, so instances cannot be created outside
	 * of this class.
	 */
	private ApplicationParameters() {
	}
	
	// methods
	// ........................................................................
	
	/**
	 * This static method is the only way <code>ApplicationParameters</code>
	 * can be accessed.
	 * 
	 * @return The instance of the <code>ApplicationParameters</code>.
	 */
	public static ApplicationParameters getInstance() {
		
		if (instance == null) {
			
			// Create the instance of the 'ApplicationParameters'.
			//
			try {
				
				Properties properties = new Properties();
				properties.load(new FileReader("conf/application.properties"));
				
				instance = new ApplicationParameters();
				
				instance.build = properties.getProperty("build");
				instance.name = properties.getProperty("name");
				instance.vendor = properties.getProperty("vendor");
				instance.version = properties.getProperty("version");
				
			} // try
			catch (Exception e) {
				
				Demo.terminate(e, 1);
				
			} // catch
			
		} // if
		
		return instance;
		
	}
	
	/**
	 * Returns the build number.
	 * 
	 * @return The build number.
	 */
	public String getBuild() {
		
		return this.build;
		
	}

	/**
	 * Returns the application name.
	 * 
	 * @return The application name.
	 */
	public String getName() {
		
		return this.name;
		
	}

	/**
	 * Returns the vendor of this application.
	 * 
	 * @return The vendor name.
	 */
	public String getVendor() {
		
		return this.vendor;
		
	}

	/**
	 * Returns the version number.
	 * 
	 * @return The version number.
	 */
	public String getVersion() {
		
		return this.version;
		
	}

}
