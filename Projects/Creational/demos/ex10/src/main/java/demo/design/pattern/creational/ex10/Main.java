/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex10;

import static demo.design.pattern.creational.ex10.Person.DIVERSE;
import static demo.design.pattern.creational.ex10.Person.FEMALE;
import static demo.design.pattern.creational.ex10.Person.MALE;

import demo.util.Demo;

/**
 * A demo, how to use a prototype.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	private void runDemo() {

		Demo.log("Running demo ...");
		
		final Person linda = FEMALE.create("Linda");
		final Person eric  = MALE.create("Eric");
		final Person kim   = DIVERSE.create("Kim");
		
		Demo.log(linda.toString());
		Demo.log(eric.toString());
		Demo.log(kim.toString());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
