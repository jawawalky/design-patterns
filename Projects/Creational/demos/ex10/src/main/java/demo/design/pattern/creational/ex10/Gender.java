package demo.design.pattern.creational.ex10;

/**
 * The sexes.
 * 
 * @author Franz Tost
 *
 */
public enum Gender {
	
	// values
	// ........................................................................
	
	MALE, FEMALE, DIVERSE;

}
