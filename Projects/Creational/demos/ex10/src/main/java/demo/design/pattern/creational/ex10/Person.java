package demo.design.pattern.creational.ex10;

import demo.util.Demo;

/**
 * A clonable person.
 * 
 * @author Franz Tost
 *
 */
public class Person implements Cloneable {
	
	// constants /////
	
	// Prototypes
	
	public static final Person MALE    = new Person(Gender.MALE);
	public static final Person FEMALE  = new Person(Gender.FEMALE);
	public static final Person DIVERSE = new Person(Gender.DIVERSE);
	
	
	// fields /////
	
	private String name;
	private Gender gender;

	
	// constructors /////
	
	private Person(final Gender gender) {
		
		super();
		
		this.gender = gender;
		
	}

	
	// methods /////
	
	public Person create(final String name) {
		
		final Person person = (Person) this.clone();
		person.name = name;
		return person;
		
	}
	
	@Override
	protected Object clone() {
		
		try {
			
			return super.clone();
			
		} // try
		catch (CloneNotSupportedException e) {
			
			Demo.terminate(e, 0);
			return null;
			
		} // catch
		
	}

	@Override public String toString() {
		
		return this.getName() + " (" + this.gender +")";
		
	}

	public String getName()   { return this.name;   }
	public Gender getGender() { return this.gender; }
	
}
