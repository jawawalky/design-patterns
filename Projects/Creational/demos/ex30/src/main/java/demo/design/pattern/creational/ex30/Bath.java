package demo.design.pattern.creational.ex30;

/**
 * A bath room.
 * 
 * @author Franz Tost
 *
 */
public class Bath extends Room {
	
	// constructors
	// ........................................................................
	
	public Bath(double size) {
		
		super(size);
		
	}

}
