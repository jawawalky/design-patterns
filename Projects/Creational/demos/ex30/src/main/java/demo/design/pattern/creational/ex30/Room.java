package demo.design.pattern.creational.ex30;

/**
 * A base class for all room types.
 * 
 * @author Franz Tost
 *
 */
public abstract class Room {
	
	// fields
	// ........................................................................
	
	private double size;
	
	// constructors
	// ........................................................................
	
	public Room(double size) {
		
		super();
		
		this.size = size;
		
	}

	// methods
	// ........................................................................
	
	public double getSize() {
		
		return this.size;
		
	}
	
}
