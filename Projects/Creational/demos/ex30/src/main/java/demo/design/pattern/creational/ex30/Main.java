/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex30;

import demo.util.Demo;

/**
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log(new HouseBuilder("House A")
			.createHouseA()
			.getHouse()
			.toString());
		
		Demo.log(new HouseBuilder("House B")
			.createHouseB()
			.getHouse()
			.toString());
		
		Demo.log(new HouseBuilder("House C")
			.createHouseC()
			.getHouse()
			.toString());
		
		Demo.log(new HouseBuilder("My House")
			.addKitchen(17.39)
			.addLivingRoom(35.44)
			.addDormitory(16.92)
			.addDormitory(15.55)
			.addBath(14.02)
			.getHouse()
			.toString());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
