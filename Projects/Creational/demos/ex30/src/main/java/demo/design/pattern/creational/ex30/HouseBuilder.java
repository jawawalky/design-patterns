package demo.design.pattern.creational.ex30;

/**
 * Builds houses with different rooms.
 * 
 * @author Franz Tost
 */
public class HouseBuilder {
	
	// fields
	// ........................................................................
	
	private House house;
	
	// constructors
	// ........................................................................
	
	public HouseBuilder(String name) {
		
		super();
		
		this.house = new House(name);
		
	}
	
	// methods
	// ........................................................................
	
	public HouseBuilder createHouseA() {
		
		this.house.removeRooms();
		return this.addKitchen(15.0)
			.addBath(8.5)
			.addLivingRoom(22.75)
			.addDormitory(12.78)
			.addDormitory(10.22);
		
	}

	public HouseBuilder createHouseB() {
		
		this.house.removeRooms();
		return this.addKitchen(17.76)
			.addBath(10.33)
			.addBath(4.2)
			.addLivingRoom(25.4)
			.addDormitory(13.88)
			.addDormitory(9.14)
			.addDormitory(15.96);
		
	}

	public HouseBuilder createHouseC() {
		
		this.house.removeRooms();
		return this.addKitchen(15.0)
			.addBath(8.5)
			.addLivingRoom(18.99)
			.addDormitory(9.66);
		
	}

	public HouseBuilder addKitchen(double size) {
		
		this.house.addRoom(new Kitchen(size));
		return this;
		
	}

	public HouseBuilder addLivingRoom(double size) {
		
		this.house.addRoom(new LivingRoom(size));
		return this;
		
	}

	public HouseBuilder addBath(double size) {
		
		this.house.addRoom(new Bath(size));
		return this;
		
	}

	public HouseBuilder addDormitory(double size) {
		
		this.house.addRoom(new Dormitory(size));
		return this;
		
	}

	public House getHouse() {
		
		return this.house;
		
	}

}
