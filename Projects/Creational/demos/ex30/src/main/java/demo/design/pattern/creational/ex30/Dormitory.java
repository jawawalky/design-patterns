package demo.design.pattern.creational.ex30;

/**
 * A dormitory.
 * 
 * @author Franz Tost
 *
 */
public class Dormitory extends Room {
	
	// constructors
	// ........................................................................
	
	public Dormitory(double size) {
		
		super(size);
		
	}


}
