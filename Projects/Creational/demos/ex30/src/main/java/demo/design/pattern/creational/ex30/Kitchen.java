package demo.design.pattern.creational.ex30;

/**
 * A kitchen.
 * 
 * @author Franz Tost
 *
 */
public class Kitchen extends Room {
	
	// constructors
	// ........................................................................
	
	public Kitchen(double size) {
		
		super(size);
		
	}


}
