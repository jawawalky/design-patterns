/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex02;

import java.io.FileReader;
import java.util.Properties;

import demo.util.Demo;

/**
 * A singleton which allows us to access the application parameters stored
 * in the file <code>conf/application.properties</code>.
 * 
 * @author Franz Tost
 *
 */
public class ApplicationParameters {
	
	// inner classes
	// ........................................................................
	
	/**
	 * The inner class holding the instance variable.
	 */
	private static class InstanceHolder {
		
		/**
		 * The 'ApplicationParameters' instance.
		 */
		public static final ApplicationParameters INSTANCE = new ApplicationParameters();
		
	}
	
	// fields
	// ........................................................................
	
	/**
	 * The vendor name;
	 */
	private String vendor;
	
	/**
	 * The version number;
	 */
	private String version;
	
	/**
	 * The build number;
	 */
	private String build;
	
	/**
	 * The application name;
	 */
	private String name;
	
	// constructors
	// ........................................................................
	
	/**
	 * A private default constructor, so instances cannot be created outside
	 * of this class.
	 */
	private ApplicationParameters() {
		
		Demo.log("Creating instance ...");
		
		// Create the instance of the 'ApplicationParameters'.
		//
		try {
			
			Properties properties = new Properties();
			properties.load(new FileReader("conf/application.properties"));
			
			this.build = properties.getProperty("build");
			this.name = properties.getProperty("name");
			this.vendor = properties.getProperty("vendor");
			this.version = properties.getProperty("version");
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			
		} // catch
		
	}
	
	// methods
	// ........................................................................
	
	/**
	 * This static method is the only way <code>ApplicationParameters</code>
	 * can be accessed.
	 * 
	 * @return The instance of the <code>ApplicationParameters</code>.
	 */
	public static ApplicationParameters getInstance() {
		
		return InstanceHolder.INSTANCE;
		
	}
	
	/**
	 * Returns the build number.
	 * 
	 * @return The build number.
	 */
	public String getBuild() {
		
		return this.build;
		
	}

	/**
	 * Returns the application name.
	 * 
	 * @return The application name.
	 */
	public String getName() {
		
		return this.name;
		
	}

	/**
	 * Returns the vendor of this application.
	 * 
	 * @return The vendor name.
	 */
	public String getVendor() {
		
		return this.vendor;
		
	}

	/**
	 * Returns the version number.
	 * 
	 * @return The version number.
	 */
	public String getVersion() {
		
		return this.version;
		
	}

}
