/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex02;

import demo.util.Demo;

/**
 * This demo shows you, how the <i>Singleton</i> design pattern guarantees
 * that only one instance of a certain class is creates in the entire
 * application.<br/>
 * Note that the instance is only created, when it is needed. The inner class
 * allows us to ??? the <code>if</code>-statement in the
 * <code>getInstance()</code> method.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		// Access the singleton instance
		//
		ApplicationParameters applicationParameters = ApplicationParameters.getInstance();
		Demo.log("Application Parameters:");
		Demo.log("  Name:    " + applicationParameters.getName());
		Demo.log("  Version: " + applicationParameters.getVersion());
		Demo.log("  Build:   " + applicationParameters.getBuild());
		Demo.log("  Vendor:  " + applicationParameters.getVendor());
		
		// Check that there is just one instance
		//
		Demo.log("Only one instance:");
		Demo.log("  " + (ApplicationParameters.getInstance() == ApplicationParameters.getInstance()));

		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
