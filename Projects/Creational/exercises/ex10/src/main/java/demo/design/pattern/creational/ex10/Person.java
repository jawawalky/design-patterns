package demo.design.pattern.creational.ex10;

import demo.util.Demo;

/**
 * A clonable person.
 * 
 * @author Franz Tost
 *
 */
public class Person implements Cloneable {
	
	// constants /////
	
	// Prototypes
	
	// TODO
	//
	//  o Create prototypes for the genders MALE, FEMALE and DIVERSE.
	//
	
	
	// fields /////
	
	private String name;
	private Gender gender;

	
	// constructors /////
	
	private Person(final Gender gender) {
		
		super();
		
		this.gender = gender;
		
	}

	
	// methods /////
	
	// TODO
	//
	//  o Write a method 'Person create(String name)', which clones the current
	//    object and give the clone the specified name.
	//
	
	@Override
	protected Object clone() {
		
		try {
			
			return super.clone();
			
		} // try
		catch (CloneNotSupportedException e) {
			
			Demo.terminate(e, 0);
			return null;
			
		} // catch
		
	}

	@Override public String toString() {
		
		return this.getName() + " (" + this.gender +")";
		
	}

	public String getName()   { return this.name;   }
	public Gender getGender() { return this.gender; }
	
}
