/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex01;


/**
 * A singleton which allows us to access the application parameters stored
 * in the file <code>conf/application.properties</code>.
 * 
 * @author Franz Tost
 *
 */
public class ApplicationParameters {
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add a static reference to the 'ApplicationParameters' and call it
	//    'instance'.
	//
	
	/**
	 * The vendor name;
	 */
	private String vendor;
	
	/**
	 * The version number;
	 */
	private String version;
	
	/**
	 * The build number;
	 */
	private String build;
	
	/**
	 * The application name;
	 */
	private String name;
	
	// constructors
	// ........................................................................
	
	// TODO
	//
	//  o Add a private default constructor.
	//
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Create a public, static method called 'getInstance()'.
	//
	//  o Check, if the attribute 'instance' has already been assigned.
	//
	//  o If the attribute 'instance' is still 'null', then read the
	//    values from the property file 'conf/application.properties' and
	//    assign them to the attributes of the instance.
	//
	//    Note:
	//
	//      Use the class 'java.util.Properties' to read the property file.
	//
	
	/**
	 * Returns the build number.
	 * 
	 * @return The build number.
	 */
	public String getBuild() {
		
		return this.build;
		
	}

	/**
	 * Returns the application name.
	 * 
	 * @return The application name.
	 */
	public String getName() {
		
		return this.name;
		
	}

	/**
	 * Returns the vendor of this application.
	 * 
	 * @return The vendor name.
	 */
	public String getVendor() {
		
		return this.vendor;
		
	}

	/**
	 * Returns the version number.
	 * 
	 * @return The version number.
	 */
	public String getVersion() {
		
		return this.version;
		
	}

}
