/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex22;

/**
 * The interface that calculator factories must implement.
 * 
 * @author Franz Tost
 *
 */
public interface CalculatorFactory {
	
	// methods
	// ........................................................................
	
	/**
	 * Creates a new calculator.
	 * 
	 * @param model the model to be created.
	 * 
	 * @return The new calculator.
	 */
	Calculator create();
	
}
