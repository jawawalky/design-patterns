/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex22;

import demo.util.Demo;

/**
 * This demo shows you, how different factories may add flexibility or
 * speed up the performance of your program.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		CalculatorFactory factory = null;
		
		// TODO
		//
		//  o Create instances of the three different factory implementations.
		//
		//  o Call the method 'performCalculations(CalculatorFactory)' for
		//    all three instances.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Creates the {@link Calculator} objects and performs the calculations.
	 * 
	 * @param factory the factory.
	 */
	private void performCalculations(CalculatorFactory factory) {
		
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			
			Calculator calculator = factory.create();
			
			calculator.add(2, 5);
			calculator.sub(3, 4);
			calculator.mlt(6, 3);
			calculator.div(7, 4);
			
		} // for
		
		long endTime = System.currentTimeMillis();
		
		Demo.log("The calculations took " + (endTime - startTime) + " ms.");
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
