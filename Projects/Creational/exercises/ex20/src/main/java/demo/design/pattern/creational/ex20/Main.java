/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex20;

import demo.util.Demo;

/**
 * This demo shows you, how the <i>Factory</i> design pattern adds flexibility
 * to your application. What kind of objects will be created, can be configured
 * outside your programm.<br/>
 * Furthermore you can easily add new factory implementations without changing
 * the code of your application.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Use 'Demo.getProperty()' to read the property 'car.factory' from
		//    the property file 'demo.properties'.
		//
		//  o Use reflection to create an instance of the factory class and
		//    return the instance.
		//
		//  o Create some cars with the factory and print them on the console.
		//
		//  o Change the factory in the 'demo.properties' file.
		//

		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
