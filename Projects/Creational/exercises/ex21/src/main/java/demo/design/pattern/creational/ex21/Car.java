/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex21;

/**
 * The interface that car object must implement.
 * 
 * @author Franz Tost
 *
 */
public interface Car {
	
	// enums
	// ........................................................................
	
	/**
	 * The different car types.
	 */
	public static enum Type {
		
		LIMOUSINE("Limousine"),
		CABRIOLET("Cabriolet"),
		COMBI("Combi"),
		VAN("Van"),
		OFFROADER("Off-Roader"),
		SUV("Sports Utility Vehicle");
		
		private String name;
		
		private Type(String name) {
			
			this.name = name;
			
		}

		@Override
		public String toString() {
			
			return this.name;
			
		}
		
	}
	
	// methods
	// ........................................................................
	
	/**
	 * Returns the model name of the car.
	 * 
	 * @return The model name.
	 */
	String getModel();
	
	/**
	 * Returns the type of the car.
	 * 
	 * @return The type.
	 */
	Type getType();
	
}
