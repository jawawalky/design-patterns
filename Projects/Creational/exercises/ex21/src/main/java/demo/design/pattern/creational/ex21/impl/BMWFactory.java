/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.creational.ex21.impl;

import demo.design.pattern.creational.ex21.CarFactory;

/**
 * The fatory, which produces {@link X5} and {@link Z4} cars.
 * 
 * @author Franz Tost
 *
 */
public class BMWFactory implements CarFactory {
	
	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Add a 'Map<String, Class>' called 'models', in which you can store
	//    the association of the model name and the model class, e.g.
	//    "X5", 'X5.class'.
	//
	
	// constructors
	// ........................................................................
	
	/**
	 * Creates the factory, which can produce
	 * <ul>
	 * 	<li><i>X5</i> and</li>
	 * 	<li><i>Z4</i></li>
	 * </ul>
	 * cars.
	 */
	public BMWFactory() {
		
		// TODO
		//
		//  o Add associations for the existing models.
		//
		
	}

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method of the 'CarFactory' interface. Let its create
	//    a car according to the specified model name.
	//

}
