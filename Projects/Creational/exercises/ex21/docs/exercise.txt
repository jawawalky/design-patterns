The Factory Pattern
===================

  Factory methods may also have parameter lists, which influence the creation
  of the product.
  
  
A) Creating the Factory Interface
=================================

  'CarFactory':
  -------------

  1) Add to it the method
     
       'Car create(String model)'

  
B) Providing Implementation of the Factory Interface
====================================================

  'BMWFactory':
  -------------
  
  1) Add a 'Map<String, Class>' called 'models', in which you can store
     the association of the model name and the model class,
     e.g. "X5", 'X5.class'.
     
  2) Add associations for the existing models.
  
  3) Implement the method of the 'CarFactory' interface. Let its create
     a car according to the specified model name.

     
C) Use the Factories
====================

  'runDemo()':
  ------------
  
  1) Create different cars of different models.
