package demo.design.pattern.creational.ex30;

/**
 * Builds houses with different rooms.
 * 
 * @author Franz Tost
 */
public class HouseBuilder {
	
	// fields
	// ........................................................................
	
	private House house;
	
	// constructors
	// ........................................................................
	
	public HouseBuilder(String name) {
		
		super();
		
		this.house = new House(name);
		
	}
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Add a method, which adds a kitchen to the house and returns the
	//    builder object.
	//
	//  o Add a method, which adds a living room to the house and returns the
	//    builder object.
	//
	//  o Add a method, which adds a bath room to the house and returns the
	//    builder object.
	//
	//  o Add a method, which adds a dormitory to the house and returns the
	//    builder object.
	//
	//  o Add a method, which returns the completed house.
	//


	// TODO
	//
	//  o Add a builder method to the builder with the following signature
	//
	//      'public HouseBuilder createHouseA()'
	//
	//    It should create a house with the following rooms
	//
	//      o 1 kitchen     (15.0)
	//      o 1 bath        (8.5)
	//      o 1 living room (22.75)
	//      o 1 dormitory   (12.78)
	//      o 1 dormitory   (10.22)
	//
	//  o Add a builder method to the builder with the following signature
	//
	//      'public HouseBuilder createHouseB()'
	//
	//    It should create a house with the following rooms
	//
	//      o 1 kitchen     (17.76)
	//      o 1 bath        (10.33)
	//      o 1 bath        (4.2)
	//      o 1 living room (25.4)
	//      o 1 dormitory   (13.88)
	//      o 1 dormitory   (9.14)
	//      o 1 dormitory   (15.96)
	//
	//  o Add a builder method to the builder with the following signature
	//
	//      'public HouseBuilder createHouseC()'
	//
	//    It should create a house with the following rooms
	//
	//      o 1 kitchen     (15.0)
	//      o 1 bath        (8.5)
	//      o 1 living room (18.99)
	//      o dormitory     (9.66)
		
}
