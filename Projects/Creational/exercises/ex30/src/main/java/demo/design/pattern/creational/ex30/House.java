package demo.design.pattern.creational.ex30;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a house.
 * 
 * @author Franz Tost
 *
 */
public class House {
	
	// fields
	// ........................................................................
	
	private String name;
	
	private List<Room> rooms = new ArrayList<Room>();

	// constructors
	// ........................................................................
	
	public House(String name) {
		
		super();
		
		this.name = name;
		
	}
	
	// methods
	// ........................................................................
	
	public void addRoom(Room room) {
		
		this.rooms.add(room);
		
	}
	
	public void removeRooms() {
		
		this.rooms.clear();
		
	}

	public double getSize() {
		
		double size = 0.0;
		for (Room room : this.rooms) {
			
			size += room.getSize();
			
		} // for
		return size;
		
	}

	@Override
	public String toString() {
		
		int kitchenCount = 0;
		int livingRoomCount = 0;
		int dormitoryCount = 0;
		int bathCount = 0;
		
		for (Room room : this.rooms) {
			
			kitchenCount    += (room instanceof Kitchen)    ? 1 : 0;
			livingRoomCount += (room instanceof LivingRoom) ? 1 : 0;
			dormitoryCount  += (room instanceof Dormitory)  ? 1 : 0;
			bathCount       += (room instanceof Bath)       ? 1 : 0;
			
		} // for
		
		
		return new StringBuilder()
			.append(this.name + "\n")
			.append((kitchenCount    > 0) ? "Kitchens :     " + kitchenCount    + "\n" : "")
			.append((livingRoomCount > 0) ? "Living Rooms : " + livingRoomCount + "\n" : "")
			.append((dormitoryCount  > 0) ? "Dormitories :  " + dormitoryCount  + "\n" : "")
			.append((bathCount       > 0) ? "Baths :        " + bathCount       + "\n" : "")
			.append("Size :         " + this.getSize() + "m*m")
			.toString();
		
	}
	
}
