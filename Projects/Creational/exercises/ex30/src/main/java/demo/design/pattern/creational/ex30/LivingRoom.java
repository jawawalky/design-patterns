package demo.design.pattern.creational.ex30;

/**
 * A living room.
 * 
 * @author Franz Tost
 *
 */
public class LivingRoom extends Room {
	
	// constructors
	// ........................................................................
	
	public LivingRoom(double size) {
		
		super(size);
		
	}

}
