/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex02;

import java.lang.reflect.Proxy;

import demo.util.Demo;

/**
 * A simple demo, how the <code>java.lang.reflect.Proxy</code> class
 * works.
 * 
 * @author Franz Tost
 *
 */
public class Main {
	
	// constructors
	// ........................................................................
	
	private Main() {}
	
	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		Calculator calculator = new CalculatorImpl();
		Hello      hello      = new HelloImpl();
		HybridImpl hybrid     = new HybridImpl();
		
		
		Demo.log("Running normal instances ...");
		
		this.runCalculator(calculator);
		this.runHello(hello);
		
		
		Demo.log("Running proxy instances ...");
		
		this.runCalculatorProxy(calculator);
		this.runHelloProxy(hello);
		this.runHybridProxy(hybrid);
		

		Demo.log("Finished.");

	}
	
	private void runCalculator(Calculator calculator) {
		
		Demo.log("Running calculator ...");

		Demo.log("3 + 7  = " + calculator.add(3, 7));
		Demo.log("12 - 5 = " + calculator.sub(12, 5));
		Demo.log("4 * 6  = " + calculator.mlt(4, 6));
		Demo.log("28 / 7 = " + calculator.div(28, 7));
		
	}
	
	private void runHello(Hello hello) {
		
		Demo.log("Running hello ...");

		Demo.log("Answer: " + hello.sayHello());

	}
	
	private void runCalculatorProxy(Calculator calculator) {
		
		Demo.log("Running calculator proxy ...");

		Calculator calculatorProxy =
			(Calculator) Proxy.newProxyInstance(
				Calculator.class.getClassLoader(),
				new Class[] { Calculator.class },
				new LogHandler(calculator)
			);
		
		this.runCalculator(calculatorProxy);
		
	}
	
	private void runHelloProxy(Hello hello) {
		
		Demo.log("Running hello proxy ...");

		Hello helloProxy =
			(Hello) Proxy.newProxyInstance(
				Hello.class.getClassLoader(),
				new Class[] { Hello.class },
				new LogHandler(hello)
			);

		this.runHello(helloProxy);
		
	}
	
	private void runHybridProxy(HybridImpl hybrid) {
		
		Demo.log("Running hybrid proxy ...");

		Object hybridProxy =
			Proxy.newProxyInstance(
				HybridImpl.class.getClassLoader(),
				new Class[] { Hello.class, Calculator.class },
				new LogHandler(hybrid)
			);
		
		this.runCalculator((Calculator) hybridProxy);
		this.runHello((Hello) hybridProxy);
		
	}

	/**
	 * Runs the demo program.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		Main demo = new Main();
		demo.runDemo();

	}

}
