/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex02;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import demo.util.Demo;

/**
 * An invocation handler, which performs some logging before and after
 * the method call.
 * 
 * @author Franz Tost
 *
 */
public class LogHandler implements InvocationHandler {
	
	// fields
	// ........................................................................

	private Object delegate;
	
	// constructors
	// ........................................................................

	public LogHandler(final Object delegate) { this.delegate = delegate; }

	// methods
	// ........................................................................
	
	@Override public Object invoke(
		final Object   proxy,
		final Method   method,
		final Object[] arguments
	) throws Throwable {
		
		try {
			
			this.log("Entering method '" + method.getName() + "'");
			return method.invoke(this.delegate, arguments);
			
		} // try
		finally {
			
			this.log("Exiting method '" + method.getName() + "'");
			
		} // finally
		
	}

	private void log(String message) {
		
		Demo.log("[LOG]: " + message);
		
	}

}
