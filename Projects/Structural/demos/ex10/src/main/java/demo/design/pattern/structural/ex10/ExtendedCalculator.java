/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;

/**
 * The extended calculator adds the calculation of powers and faculties.
 * 
 * @author Franz Tost
 *
 */
public class ExtendedCalculator extends DecoratorBase {
	
	// constructors
	// ........................................................................

	public ExtendedCalculator(Calculator calculator) {
		
		super(calculator);
		
	}

	// methods
	// ........................................................................
	
	public double pow(double x, int n) {
		
		double y = 1;
		
		for (int i = 0; i < n; i++) {
			
			y = mlt(y, x);
			
		} // for
		
		return y;
		
	}

	public int fac(int n) {
		
		int y = 1;
		
		for (int i = 1; i <= n; i++) {
			
			y = (int) mlt(y, i);
			
		} // for
		
		return y;
		
	}

}
