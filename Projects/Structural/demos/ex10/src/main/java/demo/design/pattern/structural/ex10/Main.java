/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;

import demo.util.Demo;

/**
 * This demo shows you, how different factories may add flexibility or
 * speed up the performance of your program.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log("Basis Calculator:");
		
		Calculator basicCalc = new BasicCalculator();
		
		Demo.log("2 + 3 = " + basicCalc.add(2, 3));
		
		
		Demo.log("Extended Calculator:");
		
		ExtendedCalculator extCalc = new ExtendedCalculator(basicCalc);
		
		Demo.log("2 + 3 = " + extCalc.add(2, 3));
		Demo.log("2 ^ 3 = " + extCalc.pow(2, 3));
		Demo.log("3!    = " + extCalc.fac(3));
		
		
		Demo.log("Trigonometric Calculator:");
		
		TrigonometricCalculator trigoCalc = new TrigonometricCalculator(basicCalc);
		
		Demo.log("2 + 3     = " + trigoCalc.add(2, 3));
		Demo.log("sin(0)    = " + trigoCalc.sin(0));
		Demo.log("sin(pi/2) = " + trigoCalc.sin(Math.PI / 2));
		Demo.log("cos(0)    = " + trigoCalc.cos(0));
		Demo.log("cos(pi/2) = " + trigoCalc.cos(Math.PI / 2));
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
