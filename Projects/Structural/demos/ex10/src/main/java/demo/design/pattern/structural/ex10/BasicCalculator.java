/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;


/**
 * A basic implementation of the {@linkplain Calculator} interface.
 * 
 * @author Franz Tost
 *
 */
public class BasicCalculator implements Calculator {

	// methods
	// ........................................................................
	
	@Override public double add(double a, double b) { return a + b; }

	@Override public double sub(double a, double b) { return a - b; }

	@Override public double mlt(double a, double b) { return a * b; }

	@Override public double div(double a, double b) { return a / b; }
	
}
