/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;

/**
 * A base implementation for {@linkplain Calculator} decorators.
 * 
 * @author Franz Tost
 *
 */
public abstract class DecoratorBase implements Calculator {
	
	// fields
	// ........................................................................
	
	protected Calculator calculator;
	
	// constructors
	// ........................................................................
	
	public DecoratorBase(Calculator calculator) {
		
		super();
		
		this.calculator = calculator;
		
	}

	// methods
	// ........................................................................
	
	@Override
	public double add(double a, double b) { return this.calculator.add(a, b); }

	@Override
	public double sub(double a, double b) { return this.calculator.sub(a, b); }

	@Override
	public double mlt(double a, double b) { return this.calculator.mlt(a, b); }

	@Override
	public double div(double a, double b) { return this.calculator.div(a, b); }
	
}
