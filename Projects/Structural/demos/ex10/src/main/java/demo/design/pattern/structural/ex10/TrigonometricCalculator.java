/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;

/**
 * The trigonometric calculator adds the calculation of sine, cosine and
 * tangent values.
 * 
 * @author Franz Tost
 *
 */
public class TrigonometricCalculator extends DecoratorBase {
	
	// constructors
	// ........................................................................

	public TrigonometricCalculator(Calculator calculator) {
		
		super(new ExtendedCalculator(calculator));
		
	}

	// methods
	// ........................................................................
	
	public double sin(double x) {
		
		ExtendedCalculator calc = (ExtendedCalculator) this.calculator;
		
		double y = 0;
		
		for (int i = 0; i < 5; i++) {
			
			int n = (int) add(mlt(2, i), 1);
			
			double a = calc.pow(-1, i);
			double b = calc.pow(x, n);
			double c = calc.fac(n);
			
			y = add(y, mlt(a, div(b, c)));
			
		} // for
		
		return y;
		
	}

	public double cos(double x) {
		
		ExtendedCalculator calc = (ExtendedCalculator) this.calculator;
		
		double y = 0;
		
		for (int i = 0; i < 5; i++) {
			
			int n = (int) mlt(2, i);
			
			double a = calc.pow(-1, i);
			double b = calc.pow(x, n);
			double c = calc.fac(n);
			
			y = add(y, mlt(a, div(b, c)));
			
		} // for
		
		return y;
		
	}
	
	public double tan(double x) {
		
		return div(this.sin(x), this.cos(x));
		
	}

}
