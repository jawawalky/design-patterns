/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple log manager class.
 * 
 * @author Franz Tost
 *
 */
public class LogManager {
	
	// fields
	// ........................................................................
	
	private Map<String, Log> logs = new HashMap<String, Log>();
	
	// methods
	// ........................................................................
	
	public Log getLog(String name) {
		
		Log log = this.logs.get(name);
		if (log == null) {
			
			log = new Log(name);
			this.logs.put(name, log);
			
		} // if
		
		return log;
		
	}

}
