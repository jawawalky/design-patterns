/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import demo.util.Demo;

/**
 * A dummy class representing a print job.
 * 
 * @author Franz Tost
 *
 */
public class PrintJob {
	
	// fields
	// ........................................................................
	
	private int id;
	
	// constructors
	// ........................................................................
	
	public PrintJob(int id) {
		
		this.id = id;
		
	}

	// methods
	// ........................................................................
	
	@Override
	public String toString() {
		
		return "Print Job " + this.id;
		
	}
	
	public void print(Document document) {
		
		Demo.log(this + " is printing document ...");
		
	}

}
