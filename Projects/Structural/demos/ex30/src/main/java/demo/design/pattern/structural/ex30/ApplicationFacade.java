/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

/**
 * The application facade.
 * 
 * @author Franz Tost
 *
 */
public class ApplicationFacade {
	
	// methods
	// ........................................................................
	
	/**
	 * Logs a message.
	 * 
	 * @param name the name of the log to be used.
	 * @param message the message to be logged.
	 */
	public void log(String name, String message) {
		
		NamingService namingService = NamingService.getInstance();
		
		LogManager logManager = (LogManager) namingService.lookup("log-manager");
		Log log = logManager.getLog(name);
		log.log(message);
		
	}
	
	/**
	 * Prints a document.
	 * 
	 * @param document the document to be printed.
	 */
	public void print(Document document) {
		
		NamingService namingService = NamingService.getInstance();
		
		PrintManager printManager = (PrintManager) namingService.lookup("print-manager");
		PrintJob printJob = printManager.createPrintJob();
		printJob.print(document);
		
	}

}
