/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple transient naming service.
 * 
 * @author Franz Tost
 *
 */
public class NamingService {
	
	// fields
	// ........................................................................
	
	private static NamingService instance;
	
	private Map<String, Object> bindings = new HashMap<String, Object>();
	
	// methods
	// ........................................................................
	
	public static NamingService getInstance() {
		
		if (instance == null) {
			
			instance = new NamingService();
			
			instance.bindings.put("log-manager", new LogManager());
			instance.bindings.put("print-manager", new PrintManager());
			
		} // if
		
		return instance;
		
	}
	
	public Object lookup(String name) {
		
		return this.bindings.get(name);
		
	}

}
