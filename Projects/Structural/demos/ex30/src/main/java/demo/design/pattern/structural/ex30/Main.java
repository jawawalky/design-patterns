/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import demo.util.Demo;

/**
 * This demo shows you, how facades can simplify you API.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.runWithoutFacade();
		this.runWithFacade();
		
		Demo.log("Finished.");

	}
	
	private void runWithoutFacade() {
		
		NamingService namingService = NamingService.getInstance();
		
		LogManager logManager = (LogManager) namingService.lookup("log-manager");
		Log log = logManager.getLog("demo");
		log.log("Logging without facade!");
		
		
		Document document = new Document();
		
		PrintManager printManager = (PrintManager) namingService.lookup("print-manager");
		PrintJob printJob = printManager.createPrintJob();
		printJob.print(document);
		
	}
	
	private void runWithFacade() {
		
		ApplicationFacade facade = new ApplicationFacade();
		Document document = new Document();
		
		facade.log("demo", "Logging with facade!");
		facade.print(document);
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
