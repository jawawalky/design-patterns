/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex20;

import demo.util.Demo;

/**
 * This demo shows you, how use an adapter.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() {}

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		CalculatorA calculatorA = new CalculatorA();
		CalculatorB calculatorB = new Adapter(calculatorA);
		
		Demo.log("  7 + 3  = " + calculatorB.add(7, 3));
		Demo.log("  9 - 4  = " + calculatorB.sub(9, 4));
		Demo.log("  8 * 8  = " + calculatorB.mlt(8, 8));
		Demo.log("  15 / 5 = " + calculatorB.div(15, 5));
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
