/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex20;

import static demo.design.pattern.structural.ex20.CalculatorA.Operation.ADD;
import static demo.design.pattern.structural.ex20.CalculatorA.Operation.DIV;
import static demo.design.pattern.structural.ex20.CalculatorA.Operation.MLT;
import static demo.design.pattern.structural.ex20.CalculatorA.Operation.SUB;
import static demo.design.pattern.structural.ex20.CalculatorA.Parameter.OPERATION;
import static demo.design.pattern.structural.ex20.CalculatorA.Parameter.VALUE_1;
import static demo.design.pattern.structural.ex20.CalculatorA.Parameter.VALUE_2;

import demo.design.pattern.structural.ex20.CalculatorA.Operation;

/**
 * The adapter class, which implements {@link CalculatorB} and uses
 * {@link CalculatorA}.
 * 
 * @author Franz Tost
 *
 */
public class Adapter implements CalculatorB {
	
	// fields
	// ........................................................................
	
	private CalculatorA calculatorA;
	
	// constructors
	// ........................................................................
	
	public Adapter(final CalculatorA calculatorA) {
		
		this.calculatorA = calculatorA;
		
	}
	
	// methods
	// ........................................................................
	
	@Override public double add(final double a, final double b) {
		
		return this.performCalculation(ADD, a, b);
		
	}

	@Override public double sub(final double a, final double b) {
		
		return this.performCalculation(SUB, a, b);
		
	}
	
	@Override public double mlt(final double a, final double b) {
		
		return this.performCalculation(MLT, a, b);
		
	}

	@Override public double div(final double a, final double b) {
		
		return this.performCalculation(DIV, a, b);
		
	}

	/**
	 * This method converts the values into parameters required by
	 * calculator A.
	 * 
	 * @param operation the operation to be performed.
	 * @param a the first value.
	 * @param b the second value.
	 * 
	 * @return The result.
	 */
	private double performCalculation(Operation operation, double a, double b) {
		
		this.calculatorA.setParameter(OPERATION, operation);
		this.calculatorA.setParameter(VALUE_1, a);
		this.calculatorA.setParameter(VALUE_2, b);
		return this.calculatorA.calculate();
		
	}

}
