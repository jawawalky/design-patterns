/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex20;

/**
 * The implementation of the interface {@link CalculatorA}.
 * 
 * @author Franz Tost
 *
 */
public class CalculatorA {
	
	// enums
	// ........................................................................
	
	public static enum Parameter {
		
		OPERATION,
		VALUE_1,
		VALUE_2
		
	}
	
	public static enum Operation {
		
		ADD,
		SUB,
		MLT,
		DIV
		
	}
	
	// fields
	// ........................................................................
	
	private Operation operation;
	
	private Double value1;
	
	private Double value2;

	// methods
	// ........................................................................

	public void setParameter(Parameter parameter, Object value) {
		
		switch (parameter) {
		
		case OPERATION:
			this.operation = (Operation) value;
			break;
			
		case VALUE_1:
			this.value1 = (Double) value;
			break;
			
		case VALUE_2:
			this.value2 = (Double) value;
			break;
			
		} // if
		
	}
	
	public double calculate() {
		
		switch (this.operation) {
		
		case ADD:
			return this.value1.doubleValue() + this.value2.doubleValue();
			
		case SUB:
			return this.value1.doubleValue() - this.value2.doubleValue();
			
		case MLT:
			return this.value1.doubleValue() * this.value2.doubleValue();
			
		case DIV:
			return this.value1.doubleValue() / this.value2.doubleValue();
			
		default:
			throw new IllegalArgumentException("Unknown operation: '" + this.operation + "'!");
			
		} // switch
		
	}

}
