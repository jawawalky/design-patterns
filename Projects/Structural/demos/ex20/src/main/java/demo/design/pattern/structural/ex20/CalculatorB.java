/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex20;

/**
 * A calculator interface.
 * 
 * @author Franz Tost
 */
public interface CalculatorB {
	
	// methods
	// ........................................................................
	
	double add(double a, double b);

	double sub(double a, double b);

	double mlt(double a, double b);

	double div(double a, double b);

}
