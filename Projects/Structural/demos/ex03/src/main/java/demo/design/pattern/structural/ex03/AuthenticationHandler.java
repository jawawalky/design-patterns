/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex03;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * The authentication handler.
 * 
 * @author Franz Tost
 *
 */
public class AuthenticationHandler implements InvocationHandler {
	
	// fields
	// ........................................................................
	
	private Object delegate;

	// constructors
	// ........................................................................
	
	public AuthenticationHandler(final Object delegate) {
		
		this.delegate = delegate;
		
	}
	
	// methods
	// ........................................................................

	@Override
	public Object invoke(
		final Object   proxy,
		final Method   method, 
		final Object[] args
	) throws Throwable {
		
		AuthenticationService.checkLogin();
		return method.invoke(this.delegate, args);
		
	}

}
