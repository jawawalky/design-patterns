/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

import demo.util.Demo;

/**
 * An implementation of the graphics API, which prints information about
 * the geometric shape.
 * 
 * @author Franz Tost
 */
public class ConsoleGraphics implements GraphicsAPI {
	
	// methods
	// ........................................................................

	@Override
	public void drawCircle(double x, double y, double r) {
		
		Demo.log("Circle: (" + x + "," + y + "), r=" + r);

	}

	@Override
	public void drawRectangle(double x, double y, double w, double h) {
		
		Demo.log("Circle: (" + x + "," + y + "), w=" + w + ", h=" + h);

	}

}
