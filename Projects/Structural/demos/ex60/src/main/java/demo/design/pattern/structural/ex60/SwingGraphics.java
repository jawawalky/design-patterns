/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

import java.awt.Graphics2D;

/**
 * An interface for drawing geometric shapes.
 * 
 * @author Franz Tost
 */
public class SwingGraphics implements GraphicsAPI {
	
	// fields
	// ........................................................................
	
	private Graphics2D graphics;

	// constructors
	// ........................................................................
	
	public SwingGraphics(final Graphics2D graphics) {
		
		super();
		
		this.graphics = graphics;
		
	}
	
	// methods
	// ........................................................................

	@Override
	public void drawCircle(double x, double y, double r) {
		
		this.graphics.fillOval((int) (x - r), (int) (y - r), (int) (2 * r), (int) (2 * r));

	}

	@Override
	public void drawRectangle(double x, double y, double w, double h) {
		
		this.graphics.fillRect((int) x, (int) y, (int) w, (int) h);

	}

}
