/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

/**
 * An interface for drawing geometric shapes.
 * 
 * @author Franz Tost
 */
public interface GraphicsAPI {
	
	// methods
	// ........................................................................
	
	void drawCircle(double x, double y, double r);
	
	void drawRectangle(double x, double y, double w, double h);
	
}
