/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

/**
 * A rectangle shape.
 * 
 * @author Franz Tost
 */
public class Rectangle extends Shape {
	
	// fields
	// ........................................................................
	
	private double x;

	private double y;

	private double w;

	private double h;

	// constructors
	// ........................................................................
	
	public Rectangle(final GraphicsAPI graphics) {
		
		super(graphics);
		
	}

	// methods
	// ........................................................................
	
	@Override
	public void draw() {
		
		this.graphics.drawRectangle(x, y, w, h);

	}

	@Override
	public void stretch(double factor) {
		
		this.w *= factor;
		this.h *= factor;

	}

	@Override
	public void shift(double x, double y) {
		
		this.x += x;
		this.y += y;
		
	}

}
