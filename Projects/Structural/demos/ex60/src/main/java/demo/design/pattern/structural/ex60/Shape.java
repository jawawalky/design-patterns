/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

/**
 * A geometric shape.
 * 
 * @author Franz Tost
 */
public abstract class Shape {
	
	// fields
	// ........................................................................
	
	protected GraphicsAPI graphics;
	
	// constructors
	// ........................................................................
	
	public Shape(final GraphicsAPI graphics) {
		
		super();
		
		this.graphics = graphics;
		
	}
	
	// methods
	// ........................................................................
	
	public abstract void draw();
	
	public abstract void stretch(double factor);
	
	public abstract void shift(double x, double y);
	
}
