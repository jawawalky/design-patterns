/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2015 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex60;

/**
 * A circle shape.
 * 
 * @author Franz Tost
 */
public class Circle extends Shape {
	
	// fields
	// ........................................................................
	
	private double x;

	private double y;

	private double r;

	// constructors
	// ........................................................................
	
	public Circle(final GraphicsAPI graphics) {
		
		super(graphics);
		
	}

	// methods
	// ........................................................................
	
	@Override
	public void draw() {
		
		this.graphics.drawCircle(x, y, r);

	}

	@Override
	public void stretch(double factor) {
		
		this.r *= factor;

	}

	@Override
	public void shift(double x, double y) {
		
		this.x += x;
		this.y += y;
		
	}

}
