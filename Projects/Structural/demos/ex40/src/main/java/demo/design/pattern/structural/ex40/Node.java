/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex40;

import java.util.List;

/**
 * The basic node interface.
 * 
 * @author Franz Tost
 *
 * @param <N> the node type.
 */
@SuppressWarnings({ "rawtypes" })
public interface Node<N extends Node> {
	
	// methods
	// ........................................................................
	
	N getParent();
	
	List<N> getChildren();

}
