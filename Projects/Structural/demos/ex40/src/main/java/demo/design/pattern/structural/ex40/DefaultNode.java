package demo.design.pattern.structural.ex40;

import java.io.PrintStream;
import java.util.Arrays;

/**
 * A concrete default implementation of a tree node.
 * 
 * @author Franz Tost
 *
 * @param <D> the type of the value hold by a node.
 */
public final class DefaultNode<D> extends AbstractNode<DefaultNode<D>> {
	
	// fields
	// ........................................................................
	
	private D value;

	// constructors
	// ........................................................................
	
	public DefaultNode(DefaultNode<D> parent, D value) {
		
		super(parent);
		
		this.value = value;
		
	}

	public DefaultNode(DefaultNode<D> parent) {
		
		this(parent, null);
		
	}

	public DefaultNode(D value) {
		
		this(null, value);
		
	}

	// methods
	// ........................................................................
	
	public D getValue() {
		
		return this.value;
		
	}

	public void setValue(D value) {
		
		this.value = value;
		
	}
	
	public int depth() {
		
		if (this.getParent() == null) {
			
			return 0;
			
		} // if
		
		return this.getParent().depth() + 1;
		
	}
	
	public void print(PrintStream out) {
		
		int indent = 2 * this.depth();
		char[] chars = new char[indent];
		Arrays.fill(chars, ' ');
		
		if (indent >= 2) {
			
			chars[indent - 2] = '+';
			chars[indent - 1] = '-';
			      
		} // if
		
		out.println(new String(chars) + this.getValue().toString());
		
		for (DefaultNode<D> child : this.getChildren()) {
			
			child.print(out);
			
		} // for
		
	}

}
