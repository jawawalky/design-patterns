package demo.design.pattern.structural.ex40;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An abstract base implementation for node classes.
 * 
 * @author Franz Tost
 *
 * @param <N> the node type.
 */
@SuppressWarnings({ "rawtypes" })
public abstract class AbstractNode<N extends AbstractNode> implements Node<N> {
	
	// fields
	// ........................................................................
	
	private N parent;
	
	protected List<N> children = new ArrayList<N>();

	// constructors
	// ........................................................................
	
	@SuppressWarnings("unchecked")
	public AbstractNode(N parent) {
		
		super();
		
		if (parent != null) {
			
			this.parent = parent;
			parent.children.add(this);
			
		} // if
		
	}

	// methods
	// ........................................................................

	@Override
	public N getParent() {
		
		return this.parent;
		
	}

	@Override
	public List<N> getChildren() {
		
		return Collections.unmodifiableList(this.children);
		
	}

}
