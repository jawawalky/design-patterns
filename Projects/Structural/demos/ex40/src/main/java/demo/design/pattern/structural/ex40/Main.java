/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex40;

import demo.util.Demo;

/**
 * This demo shows you the <i>Composite</i> pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		
		Demo.log("Creating tree structure ...");
		
		DefaultNode<String> root = new DefaultNode<String>("ROOT");
		
		DefaultNode<String> a = new DefaultNode<String>(root, "A");
		DefaultNode<String> b = new DefaultNode<String>(root, "B");
		DefaultNode<String> c = new DefaultNode<String>(root, "C");
		
		new DefaultNode<String>(a, "1");
		new DefaultNode<String>(a, "2");
		
		new DefaultNode<String>(b, "1");
		
		new DefaultNode<String>(c, "1");
		new DefaultNode<String>(c, "2");
		new DefaultNode<String>(c, "3");
		
		
		Demo.log("Printing tree structure ...");
		
		root.print(System.out);
		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
