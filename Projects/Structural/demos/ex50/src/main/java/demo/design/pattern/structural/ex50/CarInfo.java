package demo.design.pattern.structural.ex50;

import java.awt.Color;

/**
 * 
 * @author Franz Tost
 *
 */
public class CarInfo {
	
	// fields
	// ........................................................................
	
	private Color color;
	
	private String typeNumber;
	
	private double horsePower;

	// constructors
	// ........................................................................
	
	public CarInfo(
		final String typeNumber,
		final Color  color,
		final double horsePower
	) {
		
		super();
		
		this.typeNumber = typeNumber;
		this.color      = color;
		this.horsePower = horsePower;
		
	}

	// methods
	// ........................................................................

	public Color getColor() {
		
		return this.color;
		
	}

	public String getTypeNumber() {
		
		return this.typeNumber;
		
	}

	public double getHorsePower() {
		
		return this.horsePower;
		
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof CarInfo) {
			
			CarInfo info = (CarInfo) obj;
			return this.typeNumber.equals(info.typeNumber)
				&& this.color.equals(info.color)
				&& (this.horsePower == info.horsePower);
			
		} // if
		
		return false;
		
	}

	@Override
	public int hashCode() {
		
		return this.typeNumber.hashCode()
			^ this.color.hashCode()
			^ (int) this.horsePower;
		
	}

}
