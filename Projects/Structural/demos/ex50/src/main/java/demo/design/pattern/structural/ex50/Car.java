package demo.design.pattern.structural.ex50;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * A car.
 * 
 * @author Franz Tost
 */
public class Car {
	
	// fields
	// ........................................................................
	
	private static final Map<CarInfo, CarInfo> INFOS = new HashMap<>();
	
	private String serialNumber;
	
	private CarInfo info;
	
	// constructors
	// ........................................................................
	
	public Car(
		final String serialNumber,
		final String typeNumber,
		final Color  color,
		final double horsePower
	) {
		
		super();
		
		this.serialNumber = serialNumber;
		this.info = this.getInfo(typeNumber, color, horsePower);
		
	}
	
	// methods
	// ........................................................................

	private CarInfo getInfo(
		final String typeNumber,
		final Color  color,
		final double horsePower
	) {
		
		CarInfo info = new CarInfo(typeNumber, color, horsePower);
		return INFOS.computeIfAbsent(info, i -> i);
		
	}

	public String getSerialNumber() {
		
		return this.serialNumber;
		
	}

	public String getTypeNumber() {
		
		return this.info.getTypeNumber();
		
	}

	public Color getColor() {
		
		return this.info.getColor();
		
	}

	public double getHorsePower() {
		
		return this.info.getHorsePower();
		
	}
	
	public static int getCacheSize() {
		
		return INFOS.size();
		
	}

}
