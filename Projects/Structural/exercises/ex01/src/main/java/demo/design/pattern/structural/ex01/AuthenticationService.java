/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex01;

import java.util.Properties;

/**
 * The authentication service.
 * 
 * @author Franz Tost
 *
 */
public class AuthenticationService {
	
	// constants
	// ........................................................................
	
	private static final Properties USERS = new Properties();
	
	static {
		
		USERS.setProperty("newton",       "gravity");
		USERS.setProperty("einstein",     "relativity");
		USERS.setProperty("schroedinger", "quantum");
		
	}
	
	// fields
	// ........................................................................
	
	// methods
	// ........................................................................
	
	public void authenticate(
		final String username,
		final String password
	) throws AuthenticationException {
		
		String storedPassword = USERS.getProperty(username);
		
		if ((storedPassword == null) || !storedPassword.equals(password)) {
			
			throw new AuthenticationException();
			
		} // if
		
	}
	
}
