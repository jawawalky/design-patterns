/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex01;

import demo.util.Demo;

/**
 * This demo shows you, how to use the proxy pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Wrap the calculator into an 'AuthenticationProxy'.
		//
		Calculator calculator = new CalculatorService();
		
		while (true) {
			
			String op = Demo.input("Operation ['+','-','*','/' or 'quit']", "+");
			if ("quit".equalsIgnoreCase(op))	break;
			
			double a  = Demo.inputDouble("a", 1.0);
			double b  = Demo.inputDouble("b", 1.0);
			
			double c  = 0;
			
			switch (op) {
				case "+": c = calculator.add(a, b); break;
				case "-": c = calculator.sub(a, b); break;
				case "*": c = calculator.mlt(a, b); break;
				case "/": c = calculator.div(a, b); break;
			} // switch
			
			Demo.log(a + " " + op + " " + b + " = " + c);
			
		} // while
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
