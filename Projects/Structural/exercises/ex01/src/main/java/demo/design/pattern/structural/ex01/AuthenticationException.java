/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex01;

/**
 * An exception, which signals that the caller has not logged in.
 * 
 * @author Franz Tost
 *
 */
public class AuthenticationException extends RuntimeException {
	
	// constructors
	// ........................................................................

	public AuthenticationException() {
		
		super("Authentication failed!");
		
	}
	
}
