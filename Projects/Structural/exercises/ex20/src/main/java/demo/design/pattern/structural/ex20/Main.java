/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex20;

import demo.util.Demo;

/**
 * This demo shows you, how to use an adapter.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() {}

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		// TODO
		//
		//  o Create an adapter object and perform some calculations.
		//
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
