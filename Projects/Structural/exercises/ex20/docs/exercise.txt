The Adapter Pattern
===================

  The adapter pattern is used, when similar functionality is available, but
  the interfaces are different. The adapter transforms a call into the other
  interface, so the existing implementation can be used.
  
  
A) Creating the Adapter Class
=============================

  There is a working calculator 'CalculatorA'. The program should work
  with a calculator of type 'CalculatorB'. So we need to create an adapter,
  which implements the interface 'CalculatorB' and used 'CalculatorA'.
  
  
  1) Create a class 'demo.design.pattern.structural.ex20.Adapter'.
  
  2) Let the adapter implement the interface 'CalculatorB'.
     
  3) Implement the required interface methods. Let them use an instance
     of 'CalculatorA' to perform their calculations.
  
  
B) Using the Adapter
====================
  
  'runDemo()':
  ------------
  
  1) Create an adapter object and perform some calculations.
