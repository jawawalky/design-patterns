/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex03;


/**
 * The authentication proxy.
 * 
 * @author Franz Tost
 *
 */
public class AuthenticationProxy implements Calculator {
	
	// fields
	// ........................................................................
	
	private Calculator delegate;

	// constructors
	// ........................................................................
	
	public AuthenticationProxy(Calculator delegate) {
		
		this.delegate = delegate;
		
	}
	
	// methods
	// ........................................................................

	public double add(double a, double b) {
		
		AuthenticationService.checkLogin();
		return this.delegate.add(a, b);
		
	}

	public double sub(double a, double b) {
		
		AuthenticationService.checkLogin();
		return this.delegate.sub(a, b);
		
	}

	public double mlt(double a, double b) {
		
		AuthenticationService.checkLogin();
		return this.delegate.mlt(a, b);
		
	}

	public double div(double a, double b) {
		
		AuthenticationService.checkLogin();
		return this.delegate.div(a, b);
		
	}

}
