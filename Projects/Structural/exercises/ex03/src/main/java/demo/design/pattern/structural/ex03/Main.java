/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex03;

import demo.util.Demo;

/**
 * This demo shows you, how to use the proxy pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log("A) Without Authentication:");
		
		Calculator calculator = new CalculatorService();
		this.performCalculations(calculator, 3.0, 4.0);
		Demo.println();

		
		Demo.log("B) No Authentication:");
		
		Calculator proxy = new AuthenticationProxy(calculator);
		this.performCalculations(proxy, 3.0, 4.0);
		Demo.println();

		
		Demo.log("C) Correct Authentication:");
		
		AuthenticationService.login("newton", "gravity");
		this.performCalculations(proxy, 3.0, 4.0);
		AuthenticationService.logoff();
		Demo.println();

		
		Demo.log("D) Incorrect Authentication:");
		
		AuthenticationService.login("einstein", "quantum");
		this.performCalculations(proxy, 3.0, 4.0);
		AuthenticationService.logoff();
		Demo.println();
		
		Demo.log("Finished.");

	}
	
	/**
	 * Performs the calculations for the given values.
	 * 
	 * @param calculator the calculator.
	 * @param a the first value.
	 * @param b the second value.
	 */
	private void performCalculations(
		final Calculator calculator,
		final double     a,
		final double     b
	) {
		
		Demo.log("Performing calculations:");
		
		try {
			
			Demo.log("  " + a + " + " + b + " = " + calculator.add(a, b));
			Demo.log("  " + a + " - " + b + " = " + calculator.sub(a, b));
			Demo.log("  " + a + " * " + b + " = " + calculator.mlt(a, b));
			Demo.log("  " + a + " / " + b + " = " + calculator.div(a, b));
			
		} // try
		catch (AuthenticationException e) {
			
			Demo.log("  Error: " + e.getMessage());
			
		} // catch
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
