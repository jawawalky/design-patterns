/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex03;

import java.util.Properties;

/**
 * The authentication service.
 * 
 * @author Franz Tost
 *
 */
public class AuthenticationService {
	
	// constants
	// ........................................................................
	
	private static final Properties USERS = new Properties();
	
	static {
		
		USERS.setProperty("newton",       "gravity");
		USERS.setProperty("einstein",     "relativity");
		USERS.setProperty("schroedinger", "quantum");
		
	}
	
	// fields
	// ........................................................................
	
	private static String user;

	// methods
	// ........................................................................
	
	public static void login(String username, String password) {
		
		String storedPassword = USERS.getProperty(username);
		if ((storedPassword != null) && storedPassword.equals(password)) {
			
			user = username;
			
		}
		else {
			
			logoff();
			
		} // if
		
	}
	
	public static void logoff() {
		
		user = null;
		
	}
	
	public static void checkLogin() {
		
		if (user == null) {
			
			throw new AuthenticationException();
			
		} // if
		
	}

}
