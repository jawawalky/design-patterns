/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex40;

import demo.util.Demo;

/**
 * This demo shows you the <i>Composite</i> pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		
		Demo.log("Creating tree structure ...");
		
		// TODO
		//
		//  o Create the following tree structure with 'DefaultNode's
		//    containing strings.
		//
		//      ROOT
		//        +-A
		//          +-1
		//          +-2
		//        +-B
		//          +-1
		//        +-C
		//          +-1
		//          +-2
		//          +-3
		//
		//  o Print the tree structure on the console.
		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
