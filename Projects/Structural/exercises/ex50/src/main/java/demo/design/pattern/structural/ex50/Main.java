/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex50;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import demo.util.Demo;

/**
 * This demo shows you, how to use a flyweight pattern.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constants
	// ........................................................................
	
	private static final Color[] COLORS = {
		Color.RED,
		Color.GREEN,
		Color.BLUE,
		Color.YELLOW,
		Color.ORANGE,
		Color.BLACK,
		Color.WHITE
	};
	
	private static final Object[][] CAR_DATA = {
		{"DB 9",   570.0},
		{"A2",     66.0},
		{"520i",   145.0},
		{"307 SW", 127.0}
	};

	// constructors
	// ........................................................................

	private Main() { }

	// methods
	// ........................................................................

	private void runDemo() {

		Demo.log("Running demo ...");
		
		Random random = new Random();
		
		List<Car> cars = new ArrayList<Car>();
		for (int i = 0; i < 500000; i++) {
			
			int colorIndex   = random.nextInt(COLORS.length);
			int carDataIndex = random.nextInt(CAR_DATA.length);
			
			cars.add(new Car(String.valueOf(i),
				(String) CAR_DATA[carDataIndex][0],
				COLORS[colorIndex],
				(Double) CAR_DATA[carDataIndex][1]));
			
		} // for
		
		System.gc();
		Demo.log("Free Memory: " + Runtime.getRuntime().freeMemory());
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
