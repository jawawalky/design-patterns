package demo.design.pattern.structural.ex50;

import java.awt.Color;

/**
 * A car.
 * 
 * @author Franz Tost
 */
public class Car {
	
	// fields
	// ........................................................................
	
	private String serialNumber;
	
	private Color color;
	
	private String typeNumber;
	
	private double horsePower;

	// constructors
	// ........................................................................
	
	public Car(
		final String serialNumber,
		final String typeNumber,
		final Color  color,
		final double horsePower
	) {
		
		super();
		
		this.serialNumber = serialNumber;
		this.typeNumber   = typeNumber;
		this.color        = color;
		this.horsePower   = horsePower;
		
	}
	
	// methods
	// ........................................................................
	
	public String getSerialNumber() {
		
		return this.serialNumber;
		
	}

	public String getTypeNumber() {
		
		return this.typeNumber;
		
	}

	public Color getColor() {
		
		return this.color;
		
	}

	public double getHorsePower() {
		
		return this.horsePower;
		
	}
	
}
