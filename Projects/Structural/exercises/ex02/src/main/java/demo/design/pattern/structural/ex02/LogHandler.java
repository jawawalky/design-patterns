/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex02;

import demo.util.Demo;

// TODO
//
//  o Let the class implement the interface
//    'java.lang.reflect.InvocationHandler'.
//

/**
 * An invocation handler, which performs some logging before and after
 * the method call.
 * 
 * @author Franz Tost
 *
 */
public class LogHandler {
	
	// fields
	// ........................................................................

	/**
	 * The object to which method calls are delegated.
	 */
	private Object delegate;
	
	// constructors
	// ........................................................................

	/**
	 * Creates a log decorator, which logs all method entries and exits.
	 * 
	 * @param delegate the actual business object to which the method call
	 * 		is finally delegated.
	 */
	public LogHandler(Object delegate) {
		
		this.delegate = delegate;
		
	}

	// methods
	// ........................................................................
	
	// TODO
	// 
	//  o Implement the required interface method
	//
	//      'public Object invoke(Object proxy, Method method, Object[] paramValues) throws Throwable'
	//
	//  o Log the method entry. The log messages should contain the method name.
	//
	//  o Find by reflection the same method on the delegate object as was
	//    called on the proxy.
	//
	//  o Invoke that method and store the return value in a local variable.
	//
	//  o Log the method exit. The log messages should contain the method name.
	//
	//  o Returns the stored return value.
	//
	
	/**
	 * Prints a log message.
	 * 
	 * @param message the message.
	 */
	private void log(String message) {
		
		Demo.log("[LOG]: " + message);
		
	}

}
