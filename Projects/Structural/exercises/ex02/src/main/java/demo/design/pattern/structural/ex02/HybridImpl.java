/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex02;

import static demo.util.Demo.log;

public class HybridImpl implements Calculator, Hello {

	// methods
	// ........................................................................
	
	public double add(double a, double b) { log(a + " + " + b); return a + b; }

	public double sub(double a, double b) { log(a + " - " + b); return a - b; }

	public double mlt(double a, double b) { log(a + " * " + b); return a * b; }

	public double div(double a, double b) { log(a + " / " + b); return a / b; }

	public String sayHello() { log("sayHello"); return "Hello World!"; }

}
