/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import demo.util.Demo;

/**
 * A simple log class.
 * 
 * @author Franz Tost
 *
 */
public class Log {
	
	// fields
	// ........................................................................
	
	private String name;
	
	// constructors
	// ........................................................................
	
	public Log(String name) {
		
		this.name = name;
		
	}
	
	// methods
	// ........................................................................
	
	public void log(String message) {
		
		Demo.log("[" + this.name + "]: " + message);
		
	}

}
