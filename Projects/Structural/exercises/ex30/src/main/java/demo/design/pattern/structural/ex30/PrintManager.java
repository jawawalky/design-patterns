/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

/**
 * A dummy class representing a print manager.
 * 
 * @author Franz Tost
 *
 */
public class PrintManager {
	
	// fields
	// ........................................................................
	
	private static int count;
	
	// methods
	// ........................................................................
	
	public synchronized PrintJob createPrintJob() {
		
		count++;
		return new PrintJob(count);
		
	}

}
