/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex30;

import demo.util.Demo;

/**
 * This demo shows you, how facades can simplify you API.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		this.runWithoutFacade();
		this.runWithFacade();
		
		Demo.log("Finished.");

	}
	
	/**
	 * That's what you code would look like without a facade.
	 */
	private void runWithoutFacade() {
		
		NamingService namingService = NamingService.getInstance();
		
		LogManager logManager = (LogManager) namingService.lookup("log-manager");
		Log log = logManager.getLog("demo");
		log.log("Logging without facade!");
		
		
		Document document = new Document();
		
		PrintManager printManager = (PrintManager) namingService.lookup("print-manager");
		PrintJob printJob = printManager.createPrintJob();
		printJob.print(document);
		
	}
	
	/**
	 * That's the code, when you hide implementation details in a facade.
	 */
	private void runWithFacade() {
		
		// TODO
		//
		//  o Use the class 'ApplicationFacade' to do the same work as is
		//    done in the method 'runWithoutFacade()'.
		// 
		//  o Compare the complexity of the two methods implementations.
		//
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
