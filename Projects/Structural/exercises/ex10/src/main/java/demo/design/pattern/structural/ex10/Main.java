/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.structural.ex10;

import demo.util.Demo;

/**
 * This demo shows you, how different factories may add flexibility or
 * speed up the performance of your program.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors
	// ........................................................................

	/**
	 * A private default constructor.
	 */
	private Main() {
	}

	// methods
	// ........................................................................

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		Demo.log("Basis Calculator:");
		
		
		// TODO
		//
		//  o Create a 'BasicCalculator' and perform some calculation.
		//
		//  o Wrap the basic calculator into an extended calculator.
		//    Perform some calculations with basic and extended functions.
		//
		//  o Wrap the basic calculator into a trigonometric calculator.
		//    Perform some calculations with basic and trigonometric
		//    functions.
		//
		
		
		Demo.log("Finished.");

	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		Main app = new Main();
		app.runDemo();

	}

}
