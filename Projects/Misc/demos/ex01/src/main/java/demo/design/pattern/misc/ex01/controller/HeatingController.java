/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.controller;

import demo.design.pattern.misc.ex01.model.Heating;
import demo.design.pattern.misc.ex01.view.ChangeEvent;
import demo.design.pattern.misc.ex01.view.ChangeListener;

/**
 * A controller for a heating system. It is an observer to the view and
 * processes events coming from there.
 * 
 * @author Franz Tost
 *
 */
public class HeatingController implements ChangeListener {
	
	// fields /////
	
	private Heating heating;
	
	
	// constructors /////
	
	public HeatingController(final Heating heating) {
		
		super();
		
		this.heating = heating;
		
	}
	
	
	// methods /////
	
	@Override
	public void onChange(final ChangeEvent event) {
		
		switch (event) {
		
		case UP:
			this.heating.increaseTemperature();
			break;
			
		case DOWN:
			this.heating.decreaseTemperature();
			break;
			
		} // switch
		
	}

}
