/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.view;

import static demo.design.pattern.misc.ex01.model.Temperature.Unit.CELSIUS;
import static demo.design.pattern.misc.ex01.model.Temperature.Unit.FAHRENHEIT;
import static java.awt.BorderLayout.CENTER;

import javax.swing.JFrame;
import javax.swing.JLabel;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import demo.design.pattern.misc.ex01.model.Heating.ActualTemperatureEvent;
import demo.design.pattern.misc.ex01.model.Temperature;

/**
 * A view, which displays the actual temperature in a desired unit.
 * 
 * @author Franz Tost
 *
 */
public class TemperatureDisplay extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	
	// inner classes /////
	
	private class ActualTempertureListener {
		
		// methods /////

	    @Subscribe
	    public void onEvent(final ActualTemperatureEvent event) {
	    	
	    	display(event.getTemperature());
	    	
	    }
	    
	}
	
	
	// fields /////
	
	private JLabel label = new JLabel("---");
	
	private Temperature.Unit unit;
	
	private EventBus eventBus;
	
	
	// constructors /////
	
	public TemperatureDisplay(
		final EventBus         eventBus,
		final Temperature.Unit unit
	) {
		
		super();
		
		this.setTitle(
			CELSIUS.equals(unit) ? "Celsius" :
			FAHRENHEIT.equals(unit) ? "Fahrenheit" :
			"Kelvin"
		);
		this.add(this.label, CENTER);
		this.setSize(200, 75);
		
		this.eventBus = eventBus;
		this.unit     = unit;
		
		this.eventBus.register(new ActualTempertureListener());
		
		this.setVisible(true);
		
	}
	
	
	// methods /////
	
	private void display(final Temperature temperature) {
		
		this.label.setText(temperature.getTemperatureIn(this.unit).toString());
		
	}

}
