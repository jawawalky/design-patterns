/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.model;

import com.google.common.eventbus.EventBus;

import demo.util.Demo;

/**
 * This is the model of the heating system. It controls the relationship
 * between the desired temperature and the actual temperature. If the actual
 * temperature is too cold, the heating starts to heat. When the actual
 * temperature reaches the desired temperature, it stops heating.
 * Temperature changes are communicated by temperature events via an event bus.
 * 
 * @author Franz Tost
 *
 */
public class Heating extends Thread {
	
	// inner classes /////
	
	public static abstract class TemperatureEvent {
		
		// fields /////
		
		private Temperature temperature;
		
		
		// constructors /////
		
		public TemperatureEvent(final double value) {
			
			super();
			
			this.temperature = new Temperature(value);
			
		}
		
		
		// methods /////
		
		public Temperature getTemperature() {
			
			return this.temperature;
			
		}
		
	}
	
	public static class ActualTemperatureEvent extends TemperatureEvent {
		
		// constructors /////
		
		public ActualTemperatureEvent(final double value) {
			
			super(value);
			
		}
		
	}
	
	public static class DesiredTemperatureEvent extends TemperatureEvent {
		
		// constructors /////
		
		public DesiredTemperatureEvent(final double value) {
			
			super(value);
			
		}
		
	}

	
	// fields /////
	
	private double actualTemperatureInKelvin = 291.15;
	
	private double desiredTemperatureInKelvin = 294.15;
	
	private boolean heat;
	
	private EventBus eventBus;
	
	private boolean stop;
	
	
	// constructors /////
	
	public Heating(final EventBus eventBus) {
		
		super();
		
		this.eventBus = eventBus;
		
		this.start();
		
	}
	
	
	// methods /////
	
	public void increaseTemperature() {
		
		this.desiredTemperatureInKelvin += 0.1;
		this.postDesiredTemperature();
		
	}
	
	public void decreaseTemperature() {
		
		this.desiredTemperatureInKelvin -= 0.1;
		this.postDesiredTemperature();
		
	}
	
	public void turnOff() {
		
		this.stop = true;
		
	}
	
	@Override
	public void run() {
		
		while (!this.stop) {
			
			this.heat =
				(this.actualTemperatureInKelvin >= this.desiredTemperatureInKelvin)      ? false :
				(this.desiredTemperatureInKelvin - this.actualTemperatureInKelvin > 2.0) ? true  :
				this.heat;
			
			this.actualTemperatureInKelvin += this.heat ? 0.1 : -0.05;
			
			this.postActualTemperature();
			this.postDesiredTemperature();
			
			Demo.sleep(1000);
				
		} // while
		
	}
	
	private void postActualTemperature() {
		
		this.eventBus.post(
			new ActualTemperatureEvent(this.actualTemperatureInKelvin)
		);
			
	}
	
	private void postDesiredTemperature() {
		
		this.eventBus.post(
			new DesiredTemperatureEvent(this.desiredTemperatureInKelvin)
		);
			
	}
	
}
