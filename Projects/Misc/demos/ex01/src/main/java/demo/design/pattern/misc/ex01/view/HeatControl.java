/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.view;

import static demo.design.pattern.misc.ex01.view.ChangeEvent.DOWN;
import static demo.design.pattern.misc.ex01.view.ChangeEvent.UP;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.SOUTH;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import demo.design.pattern.misc.ex01.model.Heating.DesiredTemperatureEvent;
import demo.design.pattern.misc.ex01.model.Temperature;

/**
 * A view control, which lets a user turn the desired temperature of
 * the heating up or down.
 * 
 * @author Franz Tost
 *
 */
public class HeatControl extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	
	// inner classes /////
	
	private class DesiredTempertureListener {
		
		// methods /////

	    @Subscribe
	    public void onEvent(final DesiredTemperatureEvent event) {
	    	
	    	display(event.getTemperature());
	    	
	    }
	    
	}
	
	
	// fields /////
	
	private JLabel label = new JLabel("---");
	
	private JButton warmer = new JButton("Warmer");
	
	private JButton colder = new JButton("Colder");
	
	private Temperature.Unit unit;
	
	private EventBus eventBus;
	
	private List<ChangeListener> listeners = new ArrayList<>(1);
	
	
	// constructors /////
	
	public HeatControl(
		final EventBus         eventBus,
		final Temperature.Unit unit
	) {
		
		super();
		
		this.colder.addActionListener(e -> this.onColder());
		this.warmer.addActionListener(e -> this.onWarmer());
		
		final JPanel panel = new JPanel();
		panel.add(this.colder);
		panel.add(this.warmer);
		
		this.setTitle("Heat Control");
		this.add(this.label, CENTER);
		this.add(panel, SOUTH);
		this.setSize(230, 100);
			
		this.eventBus = eventBus;
		this.unit     = unit;
		
		this.eventBus.register(new DesiredTempertureListener());
		
		this.setVisible(true);
		
	}
	
	
	// methods /////
	
	public void addChangeListener(final ChangeListener listener) {
		
		this.listeners.add(listener);
		
	}
	
	public void removeChangeListener(final ChangeListener listener) {
		
		this.listeners.remove(listener);
		
	}
	
	private void display(final Temperature temperature) {
		
		this.label.setText(temperature.getTemperatureIn(this.unit).toString());
		
	}
	
	private void onColder() {
		
		this.listeners.forEach(l -> l.onChange(DOWN));
		
	}

	private void onWarmer() {
		
		this.listeners.forEach(l -> l.onChange(UP));
		
	}

}
