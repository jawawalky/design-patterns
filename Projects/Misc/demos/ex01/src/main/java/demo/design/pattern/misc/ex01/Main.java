/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01;

import static demo.design.pattern.misc.ex01.model.Temperature.Unit.CELSIUS;
import static demo.design.pattern.misc.ex01.model.Temperature.Unit.FAHRENHEIT;
import static demo.design.pattern.misc.ex01.model.Temperature.Unit.KELVIN;

import com.google.common.eventbus.EventBus;

import demo.design.pattern.misc.ex01.controller.HeatingController;
import demo.design.pattern.misc.ex01.model.Heating;
import demo.design.pattern.misc.ex01.view.HeatControl;
import demo.design.pattern.misc.ex01.view.TemperatureDisplay;
import demo.util.Demo;

/**
 * The MVC pattern allows the separation of applications, components,
 * modules etc. into three part
 * <ul>
 * 	<li><b>Model</b> = the data,</li>
 * 	<li><b>view</b> = the (visual) presentation and</li>
 * 	<li><b>Controller</b> = the logic.</li>
 * </ul>
 * If we split our application like that, then exchanging one of the parts
 * can easily be done, without affecting the other parts. The three parts
 * communicate with each other over well-defined interfaces, communication
 * channels and protocols etc.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		final EventBus eventBus = new EventBus();
		
		final Heating           heating    = new Heating(eventBus);
		final HeatingController controller = new HeatingController(heating);
		
		new TemperatureDisplay(eventBus, KELVIN);
		new TemperatureDisplay(eventBus, CELSIUS);
		new TemperatureDisplay(eventBus, FAHRENHEIT);
		
		final HeatControl heatControl = new HeatControl(eventBus, CELSIUS);
		heatControl.addChangeListener(controller);
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
