/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.model;

import java.text.DecimalFormat;

/**
 * Temperature objects can store a temperature in one of the following units
 * <ul>
 * 	<li>Celsius</li>
 * 	<li>Fahrenheit or</li>
 * 	<li>Kelvin.</li>
 * </ul>
 * 
 * @author Franz Tost
 */
public class Temperature {
	
	// inner classes /////
	
	public static enum Unit {
		
		// values /////
		
		CELSIUS("°C"),
		FAHRENHEIT("°F"),
		KELVIN("K");
		
		
		// fields /////
		
		private String symbol;
		
		
		// constructors /////
		
		private Unit(final String symbol) {
			
			this.symbol = symbol;
			
		}
		
		
		// methods /////
		
		@Override
		public String toString() {
			
			return this.symbol;
			
		}
		
	}
	
	
	// constants /////
	
	private static final DecimalFormat FORMAT = new DecimalFormat("#.00");
	
	
	// fields /////
	
	private double valueInKelvin;
	
	private Unit unit;
	
	
	// constructors /////
	
	public Temperature(final double value) {
		
		this(value, Unit.KELVIN);
		
	}
		
	public Temperature(final double value, final Unit unit) {
		
		super();
		
		this.unit = unit;
		
		switch (unit) {
		
		case CELSIUS:
			this.valueInKelvin = value + 273.15;
			break;
			
		case FAHRENHEIT:
			this.valueInKelvin = (value - 32.0) * 5.0/9.0 + 273.15;
			break;
			
		default:
			this.valueInKelvin = value;
			break;
			
		} // switch
		
	}
	
	
	// methods /////
	
	public double getValue() {
		
		return this.calcValue(this.unit);
		
	}
	
	public Unit getUnit() {
		
		return this.unit;
		
	}
	
	public Temperature getTemperatureIn(final Unit unit) {
		
		return new Temperature(this.calcValue(unit), unit);
		
	}
	
	@Override
	public String toString() {
		
		return FORMAT.format(this.getValue()) + " " + this.getUnit();
		
	}

	private double calcValue(final Unit unit) {
		
		switch (unit) {
		
		case CELSIUS:
			return this.valueInKelvin - 273.15;
			
		case FAHRENHEIT:
			return (this.valueInKelvin - 273.15) * 9.0/5.0 + 32.0;
			
		default:
			return this.valueInKelvin;
			
		} // switch
		
	}
	
}
