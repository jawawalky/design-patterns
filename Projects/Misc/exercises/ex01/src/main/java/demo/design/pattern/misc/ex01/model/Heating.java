/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.model;

import demo.util.Demo;

/**
 * This is the model of the heating system. It controls the relationship
 * between the desired temperature and the actual temperature. If the actual
 * temperature is too cold, the heating starts to heat. When the actual
 * temperature reaches the desired temperature, it stops heating.
 * Temperature changes are communicated by temperature events via an event bus.
 * 
 * @author Franz Tost
 *
 */
public class Heating extends Thread {
	
	// inner classes /////
	
	public static abstract class TemperatureEvent {
		
		// fields /////
		
		private Temperature temperature;
		
		
		// constructors /////
		
		public TemperatureEvent(final double value) {
			
			super();
			
			this.temperature = new Temperature(value);
			
		}
		
		
		// methods /////
		
		public Temperature getTemperature() {
			
			return this.temperature;
			
		}
		
	}
	
	public static class ActualTemperatureEvent extends TemperatureEvent {
		
		// constructors /////
		
		public ActualTemperatureEvent(final double value) {
			
			super(value);
			
		}
		
	}
	
	public static class DesiredTemperatureEvent extends TemperatureEvent {
		
		// constructors /////
		
		public DesiredTemperatureEvent(final double value) {
			
			super(value);
			
		}
		
	}

	
	// fields /////
	
	// TODO
	//
	//  o Equip the heating with an event bus, so it can inform others about
	//    about temperature changes.
	//
	//  o Pass the event bus to the heating, when it is created.
	//
	//    Hint: Use 'com.google.common.eventbus.EventBus'.
	//
	
	private double actualTemperatureInKelvin = 291.15;
	
	private double desiredTemperatureInKelvin = 294.15;
	
	private boolean heat;
	
	private boolean stop;
	
	
	// constructors /////
	
	public Heating() {
		
		super();
		
		this.start();
		
	}
	
	
	// methods /////
	
	public void increaseTemperature() {
		
		this.desiredTemperatureInKelvin += 0.1;
		
		// TODO
		//
		//  o Inform others that the desired temperature has been increased.
		//
		//    Hint: Use the event bus and 'DesiredTemperatureEvent'.
		//
		
	}
	
	public void decreaseTemperature() {
		
		this.desiredTemperatureInKelvin -= 0.1;
		
		// TODO
		//
		//  o Inform others that the desired temperature has been decreased.
		//
		//    Hint: Use the event bus and 'DesiredTemperatureEvent'.
		//
		
	}
	
	public void turnOff() {
		
		this.stop = true;
		
	}
	
	@Override
	public void run() {
		
		while (!this.stop) {
			
			this.heat =
				(this.actualTemperatureInKelvin >= this.desiredTemperatureInKelvin)      ? false :
				(this.desiredTemperatureInKelvin - this.actualTemperatureInKelvin > 2.0) ? true  :
				this.heat;
			
			this.actualTemperatureInKelvin += this.heat ? 0.1 : -0.05;
			
			// TODO
			//
			//  o Inform others about the current value of actual and desired
			//    temperature.
			//
			//    Hint: Use the event bus and 'DesiredTemperatureEvent' and
			//          'ActualTemperatureEvent'.
			//
			
			Demo.sleep(1000);
				
		} // while
		
	}
	
}
