/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.controller;

import demo.design.pattern.misc.ex01.model.Heating;

/**
 * A controller for a heating system. It is an observer to the view and
 * processes events coming from there.
 * 
 * @author Franz Tost
 *
 */
public class HeatingController {
	
	// TODO
	//
	//  o Let the controller be a listener to 'ChangeEvent's.
	//
	//  o It can later be registered as a listener to the 'HeatControl'.
	//
	//  o Let it perform the desired actions on the heating.
	//
	
	// fields /////
	
	private Heating heating;
	
	
	// constructors /////
	
	public HeatingController(final Heating heating) {
		
		super();
		
		this.heating = heating;
		
	}
	
	
	// methods /////
	
}
