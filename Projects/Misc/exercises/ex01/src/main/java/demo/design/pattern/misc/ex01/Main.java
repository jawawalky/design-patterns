/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01;

import com.google.common.eventbus.EventBus;

import demo.util.Demo;

/**
 * The MVC pattern allows the separation of applications, components,
 * modules etc. into three part
 * <ul>
 * 	<li><b>Model</b> = the data,</li>
 * 	<li><b>view</b> = the (visual) presentation and</li>
 * 	<li><b>Controller</b> = the logic.</li>
 * </ul>
 * If we split our application like that, then exchanging one of the parts
 * can easily be done, without affecting the other parts. The three parts
 * communicate with each other over well-defined interfaces, communication
 * channels and protocols etc.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }

	
	// methods /////

	/**
	 * The demo.
	 */
	private void runDemo() {

		Demo.log("Running demo ...");
		
		final EventBus eventBus = new EventBus();
		
		// TODO
		//
		//  o Create a 'Heating'.
		//
		//  o Create a 'HeatingController'.
		//
		//  o Create three 'TemperatureDisplay's, one for each temperature
		//    unit.
		//
		//  o Create a 'HeatControl'.
		//
		//  o Fuse all objects together, so they yield a valid MVC system.
		//
		
	}
	
	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
