/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.design.pattern.misc.ex01.view;

import static demo.design.pattern.misc.ex01.model.Temperature.Unit.CELSIUS;
import static demo.design.pattern.misc.ex01.model.Temperature.Unit.FAHRENHEIT;
import static java.awt.BorderLayout.CENTER;

import javax.swing.JFrame;
import javax.swing.JLabel;

import demo.design.pattern.misc.ex01.model.Temperature;

/**
 * A view, which displays the actual temperature in a desired unit.
 * 
 * @author Franz Tost
 *
 */
public class TemperatureDisplay extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	
	// fields /////
	
	// TODO
	//
	//  o Let the temperature display consume 'ActualTemperatureEvent's
	//    from the event bus. Add a field 'EventBus' for that purpose.
	//
	//  o Pass the event bus to the view, when it is being created.
	//
	//  o Register an event listener on the event bus, which listens for
	//    'ActualTemperatureEvent's sent by the heating.
	//
	//    Hint: An event bus listener is a class with a method annotated
	//          with '@Subscribe'.
	//
	//          Example:
	//
	//              class MyEventListener {
	//                  @Subscribe
	//                  public void onEvent(final MyEvent event) { ... }
	//              }
	//
	
	private JLabel label = new JLabel("---");
	
	private Temperature.Unit unit;
	
	
	// constructors /////
	
	public TemperatureDisplay(
		final Temperature.Unit unit
	) {
		
		super();
		
		this.setTitle(
			CELSIUS.equals(unit) ? "Celsius" :
			FAHRENHEIT.equals(unit) ? "Fahrenheit" :
			"Kelvin"
		);
		this.add(this.label, CENTER);
		this.setSize(200, 75);
		
		this.unit = unit;
		
		this.setVisible(true);
		
	}
	
	
	// methods /////
	
	private void display(final Temperature temperature) {
		
		this.label.setText(temperature.getTemperatureIn(this.unit).toString());
		
	}

}
