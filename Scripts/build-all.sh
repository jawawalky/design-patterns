#!/bin/sh

# All Projects
# ============
#
# Builds all Maven projects.
#

echo ">>> All Projects ..."

export TARGET_DIR="../Projects"

./build-project.sh "$TARGET_DIR/Utilities"
./build-group.sh "$TARGET_DIR/Behavioral/demos"
./build-group.sh "$TARGET_DIR/Behavioral/exercises"
./build-group.sh "$TARGET_DIR/Creational/demos"
./build-group.sh "$TARGET_DIR/Creational/exercises"
./build-group.sh "$TARGET_DIR/Structural/demos"
./build-group.sh "$TARGET_DIR/Structural/exercises"
./build-group.sh "$TARGET_DIR/Misc/demos"
./build-group.sh "$TARGET_DIR/Misc/exercises"

echo "<<< All Projects."

